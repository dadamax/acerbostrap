<?php
/**
 * Header del sito
 */
get_header();
while ( have_posts() ) : the_post();
	?>
    <style>
        h1, h2, h3, h4 {
            color: #3a5e3b
        }
    </style>
    <h1 class="title" style="background-color: #3a5e3b;color: #fff;"><?php the_title(); ?></h1>

	<?php
	/**
	 * Video dell'Acerbo
	 */
	get_template_part( 'orientamento', 'spot' );
	?>

    <!-- CONTENUTO CENTRALE -->
    <div id="content" class="cf col-md-8 bd-right">

        <h2>Vieni a scoprire l'Acerbo</h2>       <h3>Open Days On Line 2020 / 2021</h3>
		        <div class="col-md-3">         		<div class="jumbotron">        <h3>6 Dicembre 2020</h3>        <p class="lead">Open Day</p>        <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/prenotazione-open-day-6-dicembre-2020/" role="button">Prenota</a></p>      </div>	  </div>	  		        <div class="col-md-3">         		<div class="jumbotron">        <h3>13 Dicembre 2020</h3>        <p class="lead">Open Day</p>        <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/open-day-13-dicembre-2020/" role="button">Prenota</a></p>      </div>	  </div>	  		        <div class="col-md-3">         		<div class="jumbotron">        <h3>10 Gennaio 2021</h3>        <p class="lead">Open Day</p>        <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/prenotazione-open-day-10-gennaio-2021/" role="button">Prenota</a></p>      </div>	  </div>	  		        <div class="col-md-3">         		<div class="jumbotron">        <h3>17 Gennaio 2021</h3>        <p class="lead">Open Day</p>        <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/prenotazione-open-day-17-gennaio-2021/" role="button">Prenota</a></p>      </div>	  </div>


 
        


       






  

		<?php
		/**
		 * Contenuto pubblicato in Wordpress
		 */
		the_content();
		?>

        <h2>L'Istituto tecnico "Tito Acerbo"</h2>

        <div class="row">
            <div class="col-xs-12">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=150&width=747&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/2014/12/img-00951.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding: 20px 10px">
                        <p>Quest'anno è diverso, ma noi ci siamo!<br>						Per affrontare la particolare situazione di emergenza, senza rinunciare alla possibilità di farvi conoscere la nostra Offerta Formativa 						e  per guidarvi nella scelta della scuola superiore, il dirigente scolastico, i docenti dello Staff Orientamento e alcuni 						nostri alunni hanno deciso di rimodulare le attività di Orientamento, proponendole on line.<br> 						E' possibile prenotarsi e partecipare ad uno o più incontri on line con 						lo staff Orientamento della scuola, rivolgendo specifiche domande e chiedendo ulteriori informazioni.						Le date e gli orari degli Open days sono disponibili da subito e tante altre notizie saranno presto  pubblicate su questo sito.																																				</p>

                        <!--p><strong>Scopri l'Acerbo</strong>: <a href="/scuola/i-laboratori/">Laboratori e Strutture</a>
                            • <a href="/offerta-formativa/certificazioni/">Le certificazioni</a> • <a href="/docenti-ata/5425-alternanza-scuola-lavoro-a-s-2015-20106/">Alternanza Scuola-Lavoro</a> • <a href="/studenti/5607-attivazione-corsi-per-lampliamento-dellofferta-formativa/">I corsi formativi</a></p-->
                    </div>

                </div>
            </div>
        </div>


        <h2>I corsi di studio</h2>

        <div class="row">
            <div class="col-md-4">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_cat.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_cat.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/costruzioni-ambiente-e-territorio"
                                    class="thumb-container">Costruzioni, Ambiente e Territorio</a>
                        </h3>

                        <p>Gli ex geometri, per imparare, con l'ausilio di laboratori e insegnanti tecnici, a
                            progettare edifici e altre strutture grazie al disegno tecnico digitale CAD.</p>

                        <h4>Articolazioni</h4>
                        <ul>

                            <li>
                                <a href="/corsi-di-studio/costruzioni-ambiente-e-territorio/tecnico-delle-costruzioni-in-legno/">Tecnico
                                    delle Costruzioni in Legno</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <div class="grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/amministrazione-finanza-e-marketing"
                                    class="thumb-container">Amministrazione, finanza e
                                marketing</a>
                        </h3>

                        <p>Per acquisire competenze nel campo dei macrofenomeni economici nazionali ed
                            internazionali, della normativa civilistica e fiscali, dei sistemi e processi
                            aziendali e degli strumenti di marketing.</p>

                        <h4>Articolazioni</h4>
                        <ul>
                            <li>
                                <a href="/corsi-di-studio/amministrazione-finanza-e-marketing/sistemi-informativi-aziendali/">
                                    Sistemi Informativi Aziendali</a></li>
                            <li>
                                <a href="/corsi-di-studio/amministrazione-finanza-e-marketing/relazioni-internazionali-per-il-marketing/">Relazioni
                                    Internazionali per il Marketing</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class=" grid-item">
                    <picture>
                        <!--[if IE 9]>
                        <video
                                style="display: none;"><![endif]-->
                        <source media="(min-width: 992px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">
                        <source media="(min-width: 768px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">
                        <source media="(min-width: 414px)"
                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">
                        <!--[if IE 9]></video><![endif]--><img
                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"
                                alt="" class="lazyload img-responsive">
                    </picture>
                    <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
                        <h3 data-updated="" data-published="1443293073"><a
                                    href="/corsi-di-studio/turismo"
                                    class="thumb-container"
                            >Turismo</a>
                        </h3>

                        <p>Competenze specifiche nel comparto delle imprese del settore turistico e competenze
                            generali nel campo dei macrofenomeni economici nazionali ed internazionali, della
                            normativa civilistica e fiscale, dei sistemi aziendali.</p>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <!-- FINE CONTENUTO CENTRALE -->
<?php endwhile; ?>


    <!-- SIDEBAR DESTRA -->
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">

		<?php
		get_sidebar( 'orientamento' );
		?>

    </div>
    <!-- FINE SIDEBAR DESTRA -->


<?php
/**
 * FOOTER del sito
 */
get_footer();
?>