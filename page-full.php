<?php
/*
  Template Name: Template a tutta pagina
 */

get_header();
?>

    <div class="row">

        <div id="content" class="cf col-md-12">
            <?php while (have_posts()) :
            the_post(); ?>
            <div id="page-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

                <h1 class="title">
                    <?php the_title(); ?>
                </h1>

                <?php
                if (has_post_thumbnail() && !is_page(1437)) {
                    echo '<div class="post-thumb"><div class="thumb-inner">';
                    the_post_thumbnail('showed');
                    echo '</div></div>';
                }
                ?>
                <div class="blocchetto">
                    <div class="post-bodycopy cf">
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages(array(
                            'before' => __('<p class="post-pagination">Pages:', 'montezuma'),
                            'after' => '</p>'
                        ));
                        ?>
                    </div>

                    <?php edit_post_link(__('Edit', 'montezuma')); ?>

                </div>

                <?php comments_template('', true); ?>

            </div>
        </div>
        <?php endwhile; ?>
    </div>

<?php get_footer(); ?>