<div id="inprimopiano" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">In primo piano</h1>
    <div id="featured-outer">
        <div id="featured-inner">
            <div id="featured" class="row clearfix accordion">
				<?php
				$zaccordionPostId = array(
					ot_get_option( 'hp_post_02' ),
					ot_get_option( 'hp_post_03' ),
					ot_get_option( 'hp_post_01' ),
					ot_get_option( 'hp_post_04' ),
					ot_get_option( 'hp_post_05' ),
					ot_get_option( 'hp_post_06' )
				);
				if ( count( array_filter( array_unique( $zaccordionPostId ) ) ) === 6 ) {
					$query01 = array(
						'numberposts' => 6,
						'post_type'   => array( 'page', 'post', 'circolare' ),
						'post__in'    => $zaccordionPostId,
						'orderby'     => 'post__in'
					);
				} else {
					$query01 = array(
						'numberposts' => 6,
						'post_type'   => array( 'page', 'post', 'circolare' ),
						'meta_key'    => 'sticked',
						'meta_value'  => '1'
					);
				}
				$mypost = get_posts( $query01 );
				foreach ( $mypost as $post ) : setup_postdata( $post );
					?>
                    <div class="col-md-3">
						<?php
						if ( has_post_thumbnail() ) {
							$imgId = get_post_thumbnail_id( $post->ID );
							$imgSticked = wp_get_attachment_image_src( $imgId, 'sticked' );
							$imgShowed = wp_get_attachment_image_src( $imgId, 'showed' );
							$imgThumb = wp_get_attachment_image_src( $imgId, 'thumbnail' );
							echo '<picture>'
							     . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
							     . '<source media="(min-width: 992px)" data-srcset="' . $imgSticked[0] . '?width=' . $imgSticked[1] . '&height=' . $imgSticked[2] . '">'
							     . '<source media="(min-width: 768px)" data-srcset="' . $imgShowed[0] . '?width=' . $imgShowed[1] . '&height=' . $imgShowed[2] . '">'
							     . '<!--[if IE 9]></video><![endif]-->'
							     . '<img src="' . $imgThumb[0] . '?width=' . $imgThumb[1] . '&height=' . $imgThumb[2] . '" alt="' . trim( strip_tags( get_post_meta( $imgId, '_wp_attachment_image_alt', true ) ) ) . '" class="lazyload img-responsive hidden-xs">'
							     . '</picture>';

						} else {
							$image_attributes = wp_get_attachment_image_src( 1802, 'sticked' );
							echo '<img src="' . $image_attributes[0] . '" width="' . $image_attributes[1] . '" height="' . $image_attributes[2] . '">';
						}
						?>
                        <div class="featured-text">
                            <h2><a href="<?php the_permalink(); ?>" class="thumb-container"
                                   title="<?php the_title_attribute(); ?>"><?php the_title_attribute(); ?></a></h2>
							<?php the_excerpt(); ?>
                        </div>
                    </div>
					<?php
				endforeach;
				wp_reset_postdata();
				?>
            </div>
        </div>
    </div>
</div>