
<?php

/**
 * Header del sito
 */
get_header();
while ( have_posts() ) : the_post();
	?>
    <style>
        h1, h2, h3, h4 {
            color: #3a5e3b
        }
    </style>
    <h1 class="title" style="background-color: #3a5e3b;color: #fff;"><?php the_title(); ?></h1>


    <!-- CONTENUTO CENTRALE -->
    <div id="content" class="cf col-md-8 bd-right">

        <!-- QUI VA HTML PERSONALIZZATO -->




		<?php
		/**
		 * Contenuto pubblicato in Wordpress
		 */
		the_content();
		?>


    </div>
    <!-- FINE CONTENUTO CENTRALE -->
<?php endwhile; ?>


    <!-- SIDEBAR DESTRA -->
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">

		<?php
		get_sidebar( 'orientamento' );
		?>

    </div>
    <!-- FINE SIDEBAR DESTRA -->


<?php
/**
 * FOOTER del sito
 */
get_footer();
?>