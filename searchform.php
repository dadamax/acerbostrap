<?php if (is_search()) { ?>
    <form method="get" class="searchform" action="/serp">
        <div class="input-group">
            <label class="sr-only" for="modulodiricerca">Modulo di ricerca</label>
            <input id="modulodiricerca" type="text" class="form-control" name="q" value="<?php the_search_query(); ?>" required="required">
            <div class="input-group-btn">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search fa-lg"></i>&nbsp;<span class="hidden-xs">Cerca</span></button>
            </div>
        </div>
    </form>
<?php } elseif (is_page('serp')) { ?>

<?php } else { ?>
    <div class="visible-md-block visible-sm-block">
        <form method="get" class="searchform" action="/serp">
            <div class="input-group">
                <label class="sr-only" for="modulodiricercamobile">Modulo di ricerca</label>
                <input id="modulodiricercamobile" type="text" class="form-control input-sm" name="q" placeholder='Scrivi il testo e premi Cerca' value="" required="required">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search fa-lg"></i>&nbsp;<span class="hidden-xs">Cerca</span></button>
                </div>
            </div>
        </form>
    </div>
    <div class="hidden-md hidden-sm">
        <form method="get" class="searchform" action="/serp">
            <div class="input-group">
                <label class="sr-only" for="modulodiricerca">Modulo di ricerca</label>
                <input id="modulodiricerca" type="text" class="form-control" name="q" placeholder='Scrivi il testo e premi Cerca' value="" required="required">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search fa-lg"></i>&nbsp;<span class="hidden-xs">Cerca</span></button>
                </div>
            </div>
        </form>
    </div>
    <?php
}
