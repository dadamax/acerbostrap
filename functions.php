<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'restricted access' );
}


function acerbo_cloudinary_img( $source = '', $width = 0, $height = 0 ) {

	if ( ! $source ) {
		return false;
	}

	$template = 'https://res.cloudinary.com/itcacerbo/image/fetch/w_%s,h_%s,c_fill/%s';

	return sprintf( $template, $width, $height, $source );

}

/**
 * Visualizza il nome dell'autore linkato alla sua pagina
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return string
 */
function tbm_get_the_author( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$author_id = get_post_field( 'post_author', $post->ID );
	$text      = '<span class="author vcard"><a class="text-link" href="%1$s">%2$s</a></span>';
	$args      = array(
		esc_url( get_author_posts_url( $author_id ) ),
		esc_html( get_the_author_meta( 'display_name', $author_id ) )
	);

	return vsprintf( $text, $args );
}

function gmt_allow_iframes_filter( $allowedposttags ) {

	// Only change for users who can publish posts
	if ( ! current_user_can( 'publish_contributi' ) ) {
		return $allowedposttags;
	}

	// Allow iframes and the following attributes
	$allowedposttags['iframe'] = array(
		'align'        => true,
		'width'        => true,
		'height'       => true,
		'frameborder'  => true,
		'name'         => true,
		'src'          => true,
		'id'           => true,
		'class'        => true,
		'style'        => true,
		'scrolling'    => true,
		'marginwidth'  => true,
		'marginheight' => true,
	);

	return $allowedposttags;
}

add_filter( 'wp_kses_allowed_html', 'gmt_allow_iframes_filter' );

if ( file_exists( WP_CONTENT_DIR . '/maintenance.php' ) ) {
	require_once( WP_CONTENT_DIR . '/maintenance.php' );
	die();
}

$developer = isset( $_GET["fsc"] ) ? 'yes' : 'no';
$cleaned   = isset( $_GET["cleaned"] ) ? 'yes' : 'no';

/**
 * Aggiunge tag alla lista di tag whitelistati
 */

add_action( 'init', function () {
	global $allowedtags;
	$allowedtags['aria-hidden'] = array();
} );

/**
 * Verifica se una stringa è realmente vuota. Si usa per $post->content
 *
 * @param $str La stringa da cercare
 *
 * @return bool
 */

function acerbo_empty_content( $str ) {
	$str = strip_shortcodes( $str );

	return trim( str_replace( '&nbsp;', '', strip_tags( $str ) ) ) == '';
}

/**
 * Rimuovo hentry da post_class
 */
function acerbo_remove_hentry_class( $classes ) {
	$classes = array_diff( $classes, array( 'hentry' ) );

	return $classes;
}

add_filter( 'post_class', 'acerbo_remove_hentry_class' );

/**
 * Cambio il logo della pagina di login
 */

function acerbo_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url("data:image/svg+xml;utf8,<svg xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:cc='http://creativecommons.org/ns#' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'  xmlns='http://www.w3.org/2000/svg' xmlns:sodipodi='http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd' xmlns:inkscape='http://www.inkscape.org/namespaces/inkscape' width='1700px' height='334px' viewBox='0 0 195 80' version='1.1' id='svg3255' inkscape:version='0.48.5 r10040' xmlns:xlink='http://www.w3.org/1999/xlink'><rect id='svgEditorBackground' x='0' y='0' width='195' height='80' style='fill: none; stroke: none;'/><metadata id='metadata3523'><rdf:RDF><cc:Work rdf:about=''><dc:format>image/svg+xml</dc:format><dc:type rdf:resource='http://purl.org/dc/dcmitype/StillImage'/></cc:Work></rdf:RDF></metadata><defs id='defs3521'/><path d='m 96.5 0 0.6 0 c 4.2 2 7.2 5.9 11.4 7.9 C 111.7 8.5 114.8 7.9 118 8 143.7 8 169.3 8 195 8 l 0 55 C 130 63 65 63 0 63 L 0 8 C 25.7 8 51.3 8 77 8 80.2 8 83.4 8.3 86.6 7.7 90.4 5.8 93.1 2.4 96.5 0 z' id='path3259' inkscape:connector-curvature='0' style='fill:#878787'/><path d=' M 80 14.9 C 85.6 10.2 91.2 5.4 97 0.9 C 102.4 5 108.2 8.6 113.4 12.9 C 115 14.3 116.9 15.2 119 15 C 143.3 15 167.7 15 192 15 L 192 16 C 183.7 16 175.3 16 167 16 C 167 21.7 167 27.3 167 33 L 167 34 C 175.3 34 183.7 34 192 34 C 192 35.7 192 37.3 192 39 C 183.7 39 175.3 39 167 39 C 167 45 167 51 167 57 C 175.3 57 183.7 57 192 57 L 192 58 C 169 58 146 58 123 58 L 123 59 C 146 59 169 59 192 59 L 192 60 C 170.3 60 148.7 60 127 60 C 125.4 60 123.8 60.5 122.2 60.9 C 106.1 60.6 90 60.6 73.9 60.9 C 71.9 60.4 70 59.9 68 60 C 47.7 60.1 27.3 59.9 7 60 C 5.4 60.1 3.9 59.9 2.4 59.3 C 25.9 58.7 49.5 59.2 73 59 L 73 58 C 49.7 58 26.4 58 3 58 L 3 57 C 11.3 57 19.7 57 28 57 C 28 51.3 28 45.7 28 40 C 19.7 40 11.3 40 3 40 C 3 38.3 3 36.6 3 34.9 C 11.3 35.1 19.7 35 28 35 L 28 34 C 28 28 28 22 28 16 C 19.7 16 11.3 16 3 16 L 3.1 15 C 28.7 14.9 54.3 15.1 80 14.9 Z' id='path3263' fill='#bbbbbb'/><path fill='#868686' d=' M 83.5 16.2 C 87.5 11.9 92.8 8.9 97.1 4.8 C 102.4 8.3 107.8 11.9 112.5 16.3 L 111.6 15.8 C 106.5 13.2 102.2 9.4 97.6 6 C 93.2 9.3 89 12.8 84.4 15.7 L 83.5 16.2 Z' id='path3265'/><path fill='#ffffff' d=' M 97.6 6 C 102.2 9.4 106.5 13.2 111.6 15.8 C 102.5 16.1 93.4 16.2 84.4 15.7 C 89 12.8 93.2 9.3 97.6 6 Z' id='path3267'/><path d='m 1 9 c 28 0 56.1 0 84.1 0 l -1.2 1.1 C 56.2 10.1 29.7 10 2 10 c 0 17 0 34 0 51 23.9 -0.1 47.9 0.2 71.9 -0.1 16.1 -0.2 32.2 -0.2 48.3 0 23.6 0.3 47.2 0 70.8 0.1 0 -17 0 -34 0 -51 -27.7 0 -54 0 -81.6 0 L 110.3 9.1 C 138.3 9.1 166 9 194 9 c 0 17.7 0 35.3 0 53 C 129.7 62 65.3 62 1 62 1 44.3 1 26.7 1 9 z' id='path3269' inkscape:connector-curvature='0' style='fill:#ffffff'/><path fill='#bbbbbb' d=' M 3 11 C 29.6 11 56.2 11 82.8 11 C 81.8 12 80.7 12.9 79.6 13.9 C 54.1 14.1 28.5 14 3 14 C 3 13.2 3 11.8 3 11 Z' id='path3273'/><path fill='#bbbbbb' d=' M 112.8 11.2 C 139.2 10.8 165.6 11.1 192 11 C 192 11.8 192 13.2 192 14 C 167.7 14 143.3 14 119 14 C 116.5 14.3 114.4 12.9 112.8 11.2 Z' id='path3275'/><path fill='#ff9933' d=' M 3 16 C 11.3 16 19.7 16 28 16 C 28 22 28 28 28 34 C 19.7 34 11.3 34 3 34 C 3 28 3 22 3 16 Z' id='path3277'/><path d='m 31 16 c 14.1 0 28.3 -0.1 42.4 0.1 -2.7 1.4 -0.1 2.3 -2.2 4.5 L 68.4 19.8 C 68.4 22.9 66 27.9 66 31 c 1.7 0 3.3 0 5 0 0 -3.1 -0.2 -5.9 -0.3 -9 L 66.7 18.4 C 67.2 18.4 72.5 21 73 21 c 0 4.3 0 8.7 0 13 C 59 34 45 34 31 34 31 28 31 22 31 16 z' id='path3279' inkscape:connector-curvature='0' style='fill:#ff9933'/><path d='m 123 16 c 13.7 0 27.4 0 41 0 0 5.7 0 11.3 0 17 -13.7 0 -27.3 0 -41 0 0.4 -4.2 -1 -12.9 1 -16.8 L 124 21.4 c 0.1 3.2 0 6.4 0 9.6 1.7 0 3.3 0 5 0 0 -3.1 -0.2 -5.4 -0.3 -8.5 l -0.7 4.3 c -1.9 -0.8 -2 -0.8 -2.8 -2.5 -0.6 -0 -1.4 8.8 -2.1 8.8 z' id='path3281' inkscape:connector-curvature='0' style='fill:#ff9933'/><path fill='#ff9933' d=' M 167 16 C 175.3 16 183.7 16 192 16 C 192 21.7 192 27.3 192 33 C 183.7 33 175.3 33 167 33 C 167 27.3 167 21.7 167 16 Z' id='path3283'/><path fill='#8c8c8d' d=' M 7.5 19.9 C 8.3 20.8 9.1 21.6 10 22.4 C 10 25.2 10 28.1 10 31 C 8.3 31 6.7 31 5 31 C 5.1 28.4 5.1 25.7 4.3 23.2 C 5.3 22.1 6.4 21 7.5 19.9 Z' id='path3285'/><path fill='#8c8c8d' d=' M 32.3 23.2 C 33.4 22.1 34.5 21 35.5 19.9 C 36.4 20.8 37.2 21.6 38 22.4 C 38 25.3 38 28.1 38 31 C 36.4 31 34.7 31 33 31 C 33.1 28.4 33.1 25.7 32.3 23.2 Z' id='path3291'/><path fill='#8c8c8d' d=' M 56.3 23.2 C 57.4 22.1 58.5 21 59.5 19.9 C 60.4 20.8 61.2 21.6 62 22.4 C 62 25.3 62 28.1 62 31 C 60.4 31 58.7 31 57 31 C 57.1 28.4 57.1 25.7 56.3 23.2 Z' id='path3297'/><path d='m 78 20 c 13.3 -0 26.7 0 40 -0 l -0 4.1 c -2.4 0.1 -1.8 0 -4.4 0.1 -1 -1.1 -2 -2.1 -3 -3.2 -1.4 0 -2.7 0 -4.1 0 -0.9 1 -1.8 2 -2.9 2.9 -1.9 0.4 -2.8 -1.8 -4.1 -2.7 -3.1 -0.7 -6.3 0.6 -6.9 4 -2.2 -2.3 -4.6 -5.1 -8.2 -4 -1.2 1.1 -2.4 2.1 -3.5 3.2 0 -0.6 0 0.3 0 -0.2 -1.4 -0 -1.6 0.1 -3 0.1 C 78 23.3 78 20.9 78 20 z' id='path3299' inkscape:connector-curvature='0' style='fill:#ff9933'/><path fill='#8c8c8d' d=' M 134.4 19.9 C 135.2 20.8 136.2 21.5 137 22.4 C 137 25.3 137 28.1 137 31 C 135.3 31 133.7 31 132 31 C 132.1 28.4 132.1 25.7 131.3 23.2 C 132.3 22.1 133.4 21 134.4 19.9 Z' id='path3301'/><path fill='#8c8c8d' d=' M 142.5 19.9 C 143.3 20.8 144.2 21.6 145 22.4 C 145 25.2 145 28.1 145 31 C 143.3 31 141.7 31 140 31 C 140 28.9 140 26.9 140 24.8 C 139.8 24.5 139.3 23.7 139 23.4 C 140.2 22.2 141.3 21.1 142.5 19.9 Z' id='path3303'/><path fill='#8c8c8d' d=' M 171.5 19.9 C 172.3 20.8 173.2 21.6 174 22.4 C 174 25.3 174 28.1 174 31 C 172.3 31 170.7 31 169.1 31 C 169.1 28.4 169.1 25.7 168.3 23.2 C 169.3 22.1 170.4 21 171.5 19.9 Z' id='path3309'/><path fill='#c9cc9a' d=' M 6.8 22.1 C 10.1 22 5.5 25.2 6.8 22.1 Z' id='path3315'/><path fill='#c9cc9a' d=' M 34.8 22.1 C 38.1 22.2 33.4 25.2 34.8 22.1 Z' id='path3321'/><path fill='#c9cc9a' d=' M 58.8 22.1 C 62.1 22.2 57.4 25.1 58.8 22.1 Z' id='path3327'/><path d='m 66 22.8 c 0.5 0.9 1.1 0.6 1.7 1.5 -0.4 1.7 -0.7 3.5 -0.9 5.3 0.8 0 2.5 -0 3.3 -0 -0.2 -1.6 -0.4 -3.1 -0.6 -4.7 l -0.2 -0.9 c 0.5 -0.8 0.9 -0.3 1.5 -1.1 C 70.8 26 71 27.9 71 31 c -1.7 0 -3.3 0 -5 0 0 -3.1 0 -5.1 0 -8.2 z' id='path3329' inkscape:connector-curvature='0' style='fill:#878787'/><path fill='#ffffff' d=' M 83 24.1 C 83.9 23.3 84.8 22.3 85.9 21.9 C 87.7 21.7 88.8 23.2 90 24.1 C 90 27.1 90 30.2 90 33.3 C 87.7 33.3 85.3 33.2 83 33.2 C 83 30.2 83 27.1 83 24.1 Z' id='path3331'/><path fill='#ffffff' d=' M 94 24.1 C 95.2 23.1 96.3 21.8 97.9 21.8 C 99 22.5 100 23.3 101 24.1 C 101 27.1 101 30.2 101 33.2 C 98.7 33.2 96.3 33.2 94 33.2 C 94 30.2 94 27.1 94 24.1 Z' id='path3333'/><path fill='#ffffff' d=' M 105 24.1 C 106.2 23.2 107.2 21.7 108.9 21.8 C 110 22.5 111 23.3 112 24.1 C 112 27.1 112 30.2 112 33.2 C 109.7 33.3 107.3 33.3 105 33.3 C 105 30.2 105 27.1 105 24.1 Z' id='path3335'/><path fill='#fefbcd' d=' M 116.6 23.1 C 116.7 23 116.9 22.7 117 22.6 L 116.6 23.1 Z' id='path3337'/><path d='m 124.4 22.4 c 0.5 1 1.3 1 1 2.2 -0.2 1.7 -0.3 3.4 -0.5 5.1 1.1 -0.1 2.2 -0.2 3.3 -0.3 -0.2 -1.5 -0.5 -3 -0.8 -4.5 l -0.2 -0.9 c 0.5 -0.8 0.9 -0.7 1.4 -1.5 0.1 3.1 0.3 5.4 0.3 8.5 -1.7 0 -3.3 0 -5 0 0 -3.2 0.5 -5.4 0.4 -8.6 z' id='path3341' inkscape:connector-curvature='0' style='fill:#878787'/><path fill='#c9cc9a' d=' M 133.8 22.1 C 137.2 22.1 132.4 25.2 133.8 22.1 Z' id='path3343'/><path fill='#c9cc9a' d=' M 141.8 22.1 C 145.2 22.2 140.4 25.2 141.8 22.1 Z' id='path3345'/><path fill='#c9cc9a' d=' M 170.8 22.1 C 174.2 22.2 169.4 25.1 170.8 22.1 Z' id='path3351'/><path fill='#ffffff' d=' M 6 24.6 C 6.7 24.8 8 25 8.7 25.1 C 8.7 26.6 8.7 28 8.7 29.4 C 8 29.6 6.7 29.7 6 29.8 C 6 28.1 6 26.4 6 24.6 Z' id='path3357'/><path fill='#ffffff' d=' M 34 24.7 C 34.7 24.8 36 25 36.7 25.1 C 36.7 26.6 36.7 28 36.7 29.4 C 36 29.6 34.7 29.7 34 29.8 C 34 28.1 34 26.4 34 24.7 Z' id='path3359'/><path fill='#ffffff' d=' M 58 24.6 C 58.7 24.8 60 25 60.7 25.1 C 60.7 26.6 60.7 28 60.7 29.4 C 60 29.6 58.7 29.7 58 29.8 C 58 28.1 58 26.4 58 24.6 Z' id='path3361'/><path fill='#ffffff' d=' M 66.8 29.6 C 67 27.8 67.3 26 67.7 24.3 L 69.5 24.9 C 69.7 26.4 70 28 70.1 29.6 C 69.3 29.6 67.7 29.6 66.8 29.6 Z' id='path3363'/><path d='m 78 25 c 0.5 -0 2.6 0.1 3.1 0.1 0 2.7 0 5.3 -0 8 C 80.5 33.1 78.5 33 78 33 78 30.4 78 27.7 78 25 z' id='path3365' inkscape:connector-curvature='0' style='fill:#ffffff'/><path d='m 115 24.7 c 0.6 -0.1 1.6 -0 2.2 -0.1 L 117.9 24.7 c 0.1 2.8 0.1 5.6 0.1 8.4 -0.8 0 -2.2 0.1 -3 0.1 C 115 30.4 115 27.4 115 24.7 z' id='path3367' inkscape:connector-curvature='0' style='fill:#ffffff'/><path fill='#ffffff' d=' M 125 29.7 C 125.1 28 125.3 26.3 125.4 24.6 L 127.5 24.9 C 127.8 26.4 128 27.9 128.3 29.4 C 127.2 29.5 126.1 29.6 125 29.7 Z' id='path3369'/><path fill='#ffffff' d=' M 133 24.7 C 133.7 24.8 135 25 135.7 25.1 C 135.7 26.6 135.7 28 135.7 29.4 C 135 29.6 133.7 29.7 133 29.8 C 133 28.1 133 26.4 133 24.7 Z' id='path3371'/><path fill='#ffffff' d=' M 141 24.6 C 141.7 24.8 143 25 143.7 25.1 C 143.7 26.6 143.7 28 143.7 29.4 C 143 29.6 141.7 29.7 141 29.8 C 141 28.1 141 26.4 141 24.6 Z' id='path3373'/><path fill='#ffffff' d=' M 170 24.6 C 170.7 24.8 172 25 172.7 25.1 C 172.7 26.6 172.7 28 172.7 29.5 C 172 29.6 170.7 29.7 170 29.8 C 170 28.1 170 26.4 170 24.6 Z' id='path3375'/><path fill='#868686' d=' M 123 33 C 136.7 33 150.3 33 164 33 L 163.9 34 C 150.3 34 136.6 34 123 34 L 123 33 Z' id='path3377'/><path fill='#868686' d=' M 31 34 C 45 34 59 34 73 34 L 73 35 C 59 35 45 35 31 35 L 31 34 Z' id='path3379'/><path fill='#868686' d=' M 80.1 34.9 C 78.2 36.6 78.1 32.3 80 34.1 L 80.1 34.9 Z' id='path3381'/><path fill='#fffde6' d=' M 80 34.1 C 80.4 34.1 81.1 34 81.4 34 L 81.7 34.8 C 81.3 34.8 80.5 34.9 80.1 34.9 L 80 34.1 Z' id='path3383'/><path fill='#868686' d=' M 81.4 34 C 84.7 33.6 88 33.5 91.3 34 L 91.2 34.8 C 88.1 35.6 84.8 35.7 81.7 34.8 L 81.4 34 Z' id='path3385'/><path fill='#ffffff' d=' M 91.3 34 C 91.7 34 92.3 34 92.7 34 L 92.8 34.9 C 92.4 34.9 91.6 34.8 91.2 34.8 L 91.3 34 Z' id='path3387'/><path fill='#868686' d=' M 92.7 34 C 95.9 33.5 99.2 33.6 102.5 33.9 L 102.1 34.6 C 99.2 35.9 95.8 35.5 92.8 34.9 L 92.7 34 Z' id='path3389'/><path fill='#ffffff' d=' M 102.5 33.9 C 102.9 34 103.6 34.3 103.9 34.4 L 103.5 35.1 C 103.1 35 102.4 34.7 102.1 34.6 L 102.5 33.9 Z' id='path3391'/><path fill='#868686' d=' M 103.9 34.4 C 106.8 33 110.3 33.4 113.3 34.3 L 113.6 35 C 110.2 35.4 106.8 35.4 103.5 35.1 L 103.9 34.4 Z' id='path3393'/><path fill='#ffffff' d=' M 113.3 34.3 C 113.7 34.2 114.6 34.1 115 34.1 L 115.1 34.8 C 114.7 34.8 114 35 113.6 35 L 113.3 34.3 Z' id='path3395'/><path fill='#868686' d=' M 115 34.1 C 117.8 32.1 117.9 36.9 115.1 34.8 L 115 34.1 Z' id='path3397'/><path d='m 78 36 c 13.3 -0.4 26.7 -0.3 40 -0 0 1.7 0 3.5 0 5.2 -5 -0.5 -10.1 -1.7 -15.1 -0.5 C 99.3 40 95.5 39.9 92 40.8 87.4 39.5 82.6 40.5 78 41.3 78 39.5 78 37.8 78 36 z' id='path3399' inkscape:connector-curvature='0' style='fill:#ffffff'/><path fill='#ff9933' d=' M 123 39 C 136.7 39 150.3 39 164 39 C 164 45 164 51 164 57 C 150.3 57 136.7 57 123 57 C 123 51 123 45 123 39 Z' id='path3401' style='fill:#ef553e;fill-opacity:1'/><path fill='#ff9933' d=' M 167 39 C 175.3 39 183.7 39 192 39 C 192 45 192 51 192 57 C 183.7 57 175.3 57 167 57 C 167 51 167 45 167 39 Z' id='path3403' style='fill:#ef553e;fill-opacity:1'/><path fill='#ff9933' d=' M 3 40 C 11.3 40 19.7 40 28 40 C 28 45.7 28 51.3 28 57 C 19.7 57 11.3 57 3 57 C 3 51.3 3 45.7 3 40 Z' id='path3405' style='fill:#ef553e;fill-opacity:1'/><path fill='#ff9933' d=' M 31 40 C 45 40 59 40 73 40 C 73 45.7 73 51.3 73 57 C 59 57 45 57 31 57 C 31 51.3 31 45.7 31 40 Z' id='path3407' style='fill:#ef553e;fill-opacity:1'/><path fill='#ffffff' d=' M 83 42 C 85.3 42 87.7 42 90 42 C 90 47.1 90 52.2 90 57.3 C 87.7 57.3 85.3 57.2 83 57.2 C 83 52.1 83 47.1 83 42 Z' id='path3409'/><path d=' M 94 42 C 96.3 42 98.7 42 101 42 C 101 47.1 101 52.2 101 57.3 C 98.7 57.3 96.3 57.3 94 57.2 C 94 52.2 94 47.1 94 42 Z' id='path3411' fill='#ffffff'/><path fill='#ffffff' d=' M 105 42 C 107.3 42 109.7 42 112 42 C 112 47.1 112 52.2 112 57.3 C 109.7 57.3 107.3 57.3 105 57.2 C 105 52.2 105 47.1 105 42 Z' id='path3413'/><path fill='#8c8c8d' d=' M 127.5 41.9 C 128.3 42.8 129.2 43.6 130 44.4 C 130 47.3 130 50.1 130 53 C 128.3 53 126.7 53 125 53 C 125.1 50.4 125.1 47.7 124.3 45.2 C 125.3 44.1 126.4 43 127.5 41.9 Z' id='path3415'/><path fill='#8c8c8d' d=' M 132.3 45.2 C 133.3 44.1 134.4 43 135.6 41.9 C 136.4 42.8 137.2 43.6 138 44.4 C 138 47.3 138 50.1 138 53 C 136.3 53 134.7 53 133 53 C 133.1 50.4 133.2 47.7 132.3 45.2 Z' id='path3417'/><path fill='#8c8c8d' d=' M 143.4 41.9 C 144.3 42.8 145.1 43.6 146 44.4 C 146 47.2 146 50.1 146 53 C 144.3 53 142.7 53 141 53 C 141.1 50.4 141.1 47.7 140.3 45.2 C 141.3 44.1 142.4 43 143.4 41.9 Z' id='path3419'/><path fill='#8c8c8d' d=' M 171.5 41.9 C 172.3 42.8 173.2 43.6 174 44.4 C 174 47.3 174 50.1 174 53 C 172.3 53 170.7 53 169.1 53 C 169.1 50.4 169.1 47.7 168.3 45.2 C 169.3 44.1 170.4 43 171.5 41.9 Z' id='path3425'/><path fill='#8c8c8d' d=' M 4.3 46.2 C 5.4 45.1 6.4 44 7.5 42.9 C 8.3 43.8 9.2 44.6 10 45.4 C 10 48.3 10 51.1 10 54 C 8.3 54 6.7 54 5 54 C 5.1 51.4 5.1 48.7 4.3 46.2 Z' id='path3431'/><path fill='#8c8c8d' d=' M 33.3 46.2 C 34.4 45.1 35.4 44 36.5 42.9 C 37.4 43.7 38.2 44.6 39 45.4 C 39 48.3 39 51.1 39 54 C 37.4 54 35.7 54 34 54 C 34.1 51.4 34.1 48.7 33.3 46.2 Z' id='path3437'/><path fill='#8c8c8d' d=' M 57.3 46.2 C 58.4 45.1 59.5 44 60.6 42.9 C 61.4 43.8 62.2 44.6 63 45.4 C 63 48.3 63 51.1 63 54 C 61.3 54 59.7 54 58 54 C 58.1 51.4 58.1 48.7 57.3 46.2 Z' id='path3443'/><path fill='#8c8c8d' d=' M 68.5 42.9 C 69.3 43.8 70.2 44.6 71 45.4 C 71 48.3 71 51.1 71 54 C 69.3 54 67.7 54 66 54 C 66 51.4 66.2 48.7 65.3 46.3 C 66.3 45.1 67.4 44 68.5 42.9 Z' id='path3445'/><path fill='#c9cc9a' d=' M 126.8 44.1 C 130.2 44.2 125.3 47.1 126.8 44.1 Z' id='path3447'/><path fill='#c9cc9a' d=' M 134.8 44.1 C 138.2 44.1 133.4 47.2 134.8 44.1 Z' id='path3449'/><path fill='#c9cc9a' d=' M 142.8 44.1 C 146.2 44.2 141.4 47.1 142.8 44.1 Z' id='path3451'/><path fill='#c9cc9a' d=' M 170.8 44.1 C 174.1 44 169.5 47.2 170.8 44.1 Z' id='path3457'/><path fill='#c9cc9a' d=' M 6.8 45.1 C 10.2 45.2 5.3 48.1 6.8 45.1 Z' id='path3463'/><path fill='#c9cc9a' d=' M 35.8 45.1 C 39.1 45.1 34.4 48.2 35.8 45.1 Z' id='path3469'/><path fill='#c9cc9a' d=' M 59.8 45.2 C 63.1 45.1 58.4 48.2 59.8 45.2 Z' id='path3475'/><path fill='#c9cc9a' d=' M 67.8 45.1 C 71.2 45.2 66.4 48.1 67.8 45.1 Z' id='path3477'/><path d='m 78 45 c 0.5 -0 2.1 -0 2.5 -0 0 4 0.1 8.1 0.1 12.2 -0.5 0 -2.2 -0 -2.6 -0 -0 -4 -0 -8.1 -0 -12.1 z' id='path3479' inkscape:connector-curvature='0' style='fill:#ffffff'/><path d='m 115 45 c 0.7 0 2.2 0 3 0 0 4.1 0 8.1 -0 12.2 -0.7 -0 -2.3 0 -3 -0 C 114.9 53.2 115 49 115 45 z' id='path3481' inkscape:connector-curvature='0' style='fill:#ffffff'/><path fill='#ffffff' d=' M 126 46.6 C 126.7 46.8 128 47 128.7 47.1 C 128.7 48.6 128.7 50 128.7 51.5 C 128 51.5 126.7 51.7 126 51.8 C 126 50.1 126 48.4 126 46.6 Z' id='path3483'/><path fill='#ffffff' d=' M 134 46.6 C 134.7 46.8 136 47 136.7 47.1 C 136.7 48.6 136.7 50 136.7 51.5 C 136 51.5 134.7 51.7 134 51.8 C 134 50.1 134 48.4 134 46.6 Z' id='path3485'/><path fill='#ffffff' d=' M 142 46.6 C 142.7 46.8 144 47 144.7 47.1 C 144.7 48.6 144.7 50 144.7 51.5 C 144 51.5 142.7 51.7 142 51.8 C 142 50.1 142 48.4 142 46.6 Z' id='path3487'/><path fill='#ffffff' d=' M 170 46.6 C 170.7 46.8 172 47 172.7 47.2 C 172.7 48.6 172.7 50 172.7 51.5 C 172 51.5 170.7 51.7 170 51.8 C 170 50.1 170 48.4 170 46.6 Z' id='path3489'/><path fill='#ffffff' d=' M 6 47.6 C 6.7 47.8 8 48 8.7 48.1 C 8.7 49.6 8.7 51 8.7 52.5 C 8 52.5 6.7 52.8 6 52.9 C 6 51.1 6 49.4 6 47.6 Z' id='path3491'/><path fill='#ffffff' d=' M 35 47.6 C 35.7 47.8 37 48 37.7 48.1 C 37.7 49.6 37.7 51 37.7 52.5 C 37 52.5 35.7 52.7 35 52.8 C 35 51.1 35 49.4 35 47.6 Z' id='path3493'/><path fill='#ffffff' d=' M 59 47.7 C 59.7 47.8 61 48 61.7 48.1 C 61.7 49.6 61.7 51 61.7 52.5 C 61 52.5 59.7 52.7 59 52.8 C 59 51.1 59 49.4 59 47.7 Z' id='path3495'/><path fill='#ffffff' d=' M 67 47.7 C 67.7 47.8 69 48 69.7 48.1 C 69.7 49.6 69.7 51 69.7 52.5 C 69 52.5 67.7 52.7 67 52.8 C 67 51.1 67 49.4 67 47.7 Z' id='path3497'/><path fill='#868686' d=' M 78 57.5 C 78.8 57.7 80.3 58.1 81.1 58.3 C 81.8 59.6 82.5 59.5 82.9 58.1 C 85.5 57.3 88.4 57.3 91.1 58.2 C 91.6 59.5 92.2 59.5 92.9 58.2 C 95.8 57.3 99.1 57.3 102.1 58.1 C 102.6 59.5 103.2 59.6 103.9 58.2 C 106.8 57.3 110.1 57.3 113.1 58.1 C 113.6 59.5 114.2 59.6 114.9 58.3 C 115.7 58.1 117.2 57.7 118 57.5 C 118 58.2 118 59.5 118 60.2 C 104.7 60.5 91.3 60.4 78 60.2 C 78 59.5 78 58.2 78 57.5 Z' id='path3499'/><path fill='#fffde6' d=' M 81.1 58.3 C 81.6 58.2 82.5 58.2 82.9 58.1 C 82.5 59.5 81.8 59.6 81.1 58.3 Z' id='path3501'/><path fill='#fffde6' d=' M 91.1 58.2 C 91.5 58.2 92.4 58.2 92.9 58.2 C 92.2 59.5 91.6 59.5 91.1 58.2 Z' id='path3503'/><path fill='#fffde6' d=' M 102.1 58.1 C 102.5 58.2 103.4 58.2 103.9 58.2 C 103.2 59.6 102.6 59.5 102.1 58.1 Z' id='path3505'/><path fill='#fffde6' d=' M 113.1 58.1 C 113.5 58.2 114.4 58.2 114.9 58.3 C 114.2 59.6 113.6 59.5 113.1 58.1 Z' id='path3507'/><path fill='#868686' d=' M 71 64 C 89 64 107 64 125 64 L 125 65 C 107 65 89 65 71 65 L 71 64 Z' id='path3511'/><path fill='#868686' d=' M 69 67 C 88.3 67 107.7 67 127 67 C 127 67.5 127 68.5 127 69 C 107.7 69 88.3 69 69 69 C 69 68.5 69 67.5 69 67 Z' id='path3513'/><path fill='#868686' d=' M 66 71 C 87.3 71 108.7 71 130 71 C 130 71.8 130 73.2 130 74 C 108.7 74 87.3 74 66 74 C 66 73.2 66 71.8 66 71 Z' id='path3515'/><path fill='#868686' d=' M 62.1 76 C 85.7 76 109.3 76 133 76 C 133 77.3 133 78.7 133 80 L 61.9 80 C 61.9 78.7 62 77.3 62.1 76 Z' id='path3517'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 65.3 23 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-1'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 67.4 24.9 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-7'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 68.1 22.1 c 3.4 0 -1.4 3 0 0 z' id='path3327-4'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 48.7 23.1 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-9'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 51.2 22.1 c 3.4 0 -1.4 3 0 0 z' id='path3327-48'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 50.4 24.6 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-8'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 41.1 23.1 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-2'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 43.6 22 c 3.4 0 -1.4 3 0 0 z' id='path3327-45'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 42.8 24.5 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-5'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 41.9 46.2 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-17'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 44.8 45.1 c 3.4 0 -1.4 3 0 0 z' id='path3327-1'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 44 47.6 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-1'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 49.9 46.3 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-5'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 52.4 45.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-2'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 51.6 47.7 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-76'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 12.3 23.2 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-14'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 14.9 22.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-23'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 14.1 24.7 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-2'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 21.4 23.1 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-21'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 23.9 22.1 c 3.4 0 -1.4 3 0 0 z' id='path3327-6'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 23.1 24.6 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-85'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 12.3 46.1 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-7'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 14.8 45.1 c 3.4 0 -1.4 3 0 0 z' id='path3327-61'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 14 47.6 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-89'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 20.7 46.2 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-27'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 23.2 45.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-9'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 22.4 47.7 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-54'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 148.4 23 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-3'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 150.9 22 c 3.4 0 -1.4 3 0 0 z' id='path3327-12'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 150.1 24.5 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-3'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 157.1 22.9 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-34'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 159.6 21.8 c 3.4 0 -1.4 3 0 0 z' id='path3327-11'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 158.8 24.3 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-38'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 148.7 45.1 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-74'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 151.2 44 c 3.4 0 -1.4 3 0 0 z' id='path3327-27'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 150.4 46.6 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-79'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 156.7 44.9 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-31'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 159.2 43.9 c 3.4 0 -1.4 3 0 0 z' id='path3327-98'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 158.4 46.4 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-6'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 176.6 45.2 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-50'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 179.1 44.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-28'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 178.3 46.7 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-60'/><path style='fill:#ff9933;fill-opacity:1' inkscape:connector-curvature='0' d='m 184.6 45.3 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-24'/><path style='fill:#ff9933;fill-opacity:1' inkscape:connector-curvature='0' d='m 187.1 44.3 c 3.4 0 -1.4 3 0 0 z' id='path3327-8'/><path style='fill:#ff9933;fill-opacity:1' inkscape:connector-curvature='0' d='m 186.3 46.8 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-65'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 176.2 23.3 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-0'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 178.7 22.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-90'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 177.9 24.7 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-0'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 184.6 23.4 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-6'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 187.1 22.3 c 3.4 0 -1.4 3 0 0 z' id='path3327-13'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 186.3 24.9 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-893'/><path style='fill:#ff9933;fill-opacity:1' id='path3819' d='m72.7 16.5 c-0 0.1 -0 0.1 -0 0.2 0 0 0 0.1 0 0.1 0 0 -0.8 0 -0.8 0 l0 0 c0 -0 0 -0.1 0 -0.1 -0 -0.1 0 -0.1 -0 -0.2 0 0 0.8 0 0.8 0 z' inkscape:connector-curvature='0'/><rect style='fill:#ff9933;fill-opacity:1' id='rect3832' width='2' height='5' x='71' y='16'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 126.4 19.9 c 0.8 0.9 1.8 1.6 2.6 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0.1 -5.3 -0.7 -7.8 1 -1.1 2.1 -2.2 3.1 -3.3 z' id='path3301-4'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 125.8 22.2 c 3.4 -0 -1.4 3 0 0 z' id='path3343-4'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 125 24.7 c 0.7 0.1 2 0.3 2.7 0.5 0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3371-6'/><path style='fill:#8c8c8d' inkscape:connector-curvature='0' d='m 184.6 45.3 c 1.1 -1.1 2.2 -2.2 3.3 -3.3 0.8 0.8 1.6 1.7 2.5 2.5 0 2.9 0 5.7 0 8.6 -1.6 0 -3.3 0 -5 0 0.1 -2.6 0 -5.3 -0.8 -7.8 z' id='path3297-50-0'/><path style='fill:#c9cc9a' inkscape:connector-curvature='0' d='m 187.1 44.2 c 3.4 0 -1.4 3 0 0 z' id='path3327-28-6'/><path style='fill:#ffffff' inkscape:connector-curvature='0' d='m 186.3 46.8 c 0.7 0.1 2 0.4 2.7 0.5 -0 1.4 -0 2.9 -0 4.3 -0.7 0.1 -2 0.3 -2.7 0.4 0 -1.7 0 -3.5 0 -5.2 z' id='path3361-60-6'/><path style='fill:#bbbbbb;fill-opacity:1' id='path4650' d='m 122.6 12.9 a 4.2 4.3 0 1 1 -8.4 0 4.2 4.3 0 1 1 8.4 0 z' transform='matrix(0.8,0,0,0.8,2.4896051,1.6756958)'/></svg>") !important;
            padding-bottom: 30px !important;
            height: 100px !important;
            background-size: 520px !important;
            width: 320px !important;
        }
    </style>
<?php }

function acerbo_login_logo_url() {
	return home_url();
}

function acerbo_login_logo_url_title() {
	return 'Homepage Istituto Acerbo';
}

add_action( 'login_enqueue_scripts', 'acerbo_login_logo' );
add_filter( 'login_headerurl', 'acerbo_login_logo_url' );
add_filter( 'login_headertitle', 'acerbo_login_logo_url_title' );


/**
 * Aggiungo AMP agli altri Post Type
 */
add_action( 'amp_init', 'acerbo_amp_add_review_cpt' );
function acerbo_amp_add_review_cpt() {
	add_post_type_support( 'avviso', AMP_QUERY_VAR );
	add_post_type_support( 'circolare', AMP_QUERY_VAR );
	add_post_type_support( 'contributo', AMP_QUERY_VAR );
}

/**
 * Rimuovi gli embed di WP 4.4
 */
function my_deregister_scripts() {
	wp_deregister_script( 'wp-embed' );
}

add_action( 'wp_footer', 'my_deregister_scripts' );

/**
 * Disable the emoji's di WP 3.5
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}

add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}


/**
 * File richiesti
 */
require_once( 'inc/navwalker.php' );
require_once( 'inc/kea/kea.php' );
require_once( 'inc/doctotext.php' );
require_once( get_template_directory() . '/vendor/autoload.php' );
require_once( get_template_directory() . '/functions/acerbo-manageupload.php' );
require_once( get_template_directory() . '/functions/acerbo-custompost.php' );
require_once( get_template_directory() . '/functions/acerbo-query.php' );
require_once( get_template_directory() . '/functions/acerbo-widget.php' );
require_once( get_template_directory() . '/functions/acerbo-savepost.php' );
require_once( get_template_directory() . '/functions/acerbo-ajax.php' );
require_once( get_template_directory() . '/functions/acerbo-enqueue.php' );
require_once( get_template_directory() . '/functions/acerbo-shortcode.php' );

/**
 * Modulo per la sitemap degli allegati
 */

function acerbo_bwp_gxs_module_dir() {
	return get_template_directory() . '/bwp_gxs/';
}

add_filter( 'bwp_gxs_module_dir', 'acerbo_bwp_gxs_module_dir' );

function acerbo_bwp_gxs_add_modules() {
	global $bwp_gxs;
	$bwp_gxs->add_module( 'allegati' );
}

add_action( 'bwp_gxs_modules_built', 'acerbo_bwp_gxs_add_modules' );

function acerbo_bwp_gxs_add_rewrite_rules() {
	$my_rules = array(
		'allegati.xml' => 'index.php?gxs_module=allegati'
	);

	return $my_rules;
}

add_filter( 'bwp_gxs_rewrite_rules', 'acerbo_bwp_gxs_add_rewrite_rules' );


/**
 *
 * Riscrittura dei permalink
 *
 * @param type $wp_rewrite
 */

function acerbo_rewrite_attachment( $wp_rewrite ) {
	$new_rules                           = array();
	$new_rules['file/(\d[^/]*)/(?:.*)$'] = 'index.php?attachment_id=$matches[1]';
	$wp_rewrite->rules                   = $new_rules + $wp_rewrite->rules;
}

add_action( 'generate_rewrite_rules', 'acerbo_rewrite_attachment' );

function acerbo_attachment_link( $link, $post_id ) {
	$post = get_post( $post_id );

	return home_url( '/file/' . $post_id . '/' . sanitize_title( $post->post_title ) );
}

add_filter( 'attachment_link', 'acerbo_attachment_link', 20, 2 );


/**
 * Aggiunge un box testuale al media uploader
 */
function acerbo_media_upload_infobox() {
	?>
    <!-- Start infobox -->
    <div class="media-infobox" style="background:#ffff99; color:#555555; border: #CCCCCC 1px; border-radius: 10px 10px 10px
         10px; padding:5px;margin: 15px 5px">
        <h4 class="media-infobox-sub-title"
            style="font-family: Georgia,Times,serif; font-weight: normal; color: rgb(90, 90, 90); font-size: 1.3em; margin: 12px">
            Reminder: le dimensioni delle immagini per gli articoli dovrebbero essere <strong>778x280</strong> pixel.
        </h4>
    </div>
    <!-- Stop infobox -->

	<?php
}

add_action( 'post-upload-ui', 'acerbo_media_upload_infobox' );

/**
 * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @return string Trimmed string.
 */
function acerbo_truncate( $text, $length = 100, $options = array() ) {
	$defaults = array(
		'ellipsis' => '...',
		'exact'    => true,
		'html'     => false
	);
	if ( isset( $options['ending'] ) ) {
		$defaults['ellipsis'] = $options['ending'];
	} elseif ( ! empty( $options['html'] ) ) {
		$defaults['ellipsis'] = "\xe2\x80\xa6";
	}
	$options += $defaults;
	extract( $options );
	if ( ! function_exists( 'mb_strlen' ) ) {
		class_exists( 'Multibyte' );
	}
	if ( $html ) {
		if ( mb_strlen( preg_replace( '/<.*?>/', '', $text ) ) <= $length ) {
			return $text;
		}
		$totalLength = mb_strlen( strip_tags( $ellipsis ) );
		$openTags    = array();
		$truncate    = '';
		preg_match_all( '/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER );
		foreach ( $tags as $tag ) {
			if ( ! preg_match( '/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2] ) ) {
				if ( preg_match( '/<[\w]+[^>]*>/s', $tag[0] ) ) {
					array_unshift( $openTags, $tag[2] );
				} elseif ( preg_match( '/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag ) ) {
					$pos = array_search( $closeTag[1], $openTags );
					if ( $pos !== false ) {
						array_splice( $openTags, $pos, 1 );
					}
				}
			}
			$truncate      .= $tag[1];
			$contentLength = mb_strlen( preg_replace( '/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3] ) );
			if ( $contentLength + $totalLength > $length ) {
				$left           = $length - $totalLength;
				$entitiesLength = 0;
				if ( preg_match_all( '/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE ) ) {
					foreach ( $entities[0] as $entity ) {
						if ( $entity[1] + 1 - $entitiesLength <= $left ) {
							$left --;
							$entitiesLength += mb_strlen( $entity[0] );
						} else {
							break;
						}
					}
				}
				$truncate .= mb_substr( $tag[3], 0, $left + $entitiesLength );
				break;
			} else {
				$truncate    .= $tag[3];
				$totalLength += $contentLength;
			}
			if ( $totalLength >= $length ) {
				break;
			}
		}
	} else {
		if ( mb_strlen( $text ) <= $length ) {
			return $text;
		}
		$truncate = mb_substr( $text, 0, $length - mb_strlen( $ellipsis ) );
	}
	if ( ! $exact ) {
		$spacepos = mb_strrpos( $truncate, ' ' );
		if ( $html ) {
			$truncateCheck = mb_substr( $truncate, 0, $spacepos );
			$lastOpenTag   = mb_strrpos( $truncateCheck, '<' );
			$lastCloseTag  = mb_strrpos( $truncateCheck, '>' );
			if ( $lastOpenTag > $lastCloseTag ) {
				preg_match_all( '/<[\w]+[^>]*>/s', $truncate, $lastTagMatches );
				$lastTag  = array_pop( $lastTagMatches[0] );
				$spacepos = mb_strrpos( $truncate, $lastTag ) + mb_strlen( $lastTag );
			}
			$bits = mb_substr( $truncate, $spacepos );
			preg_match_all( '/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER );
			if ( ! empty( $droppedTags ) ) {
				if ( ! empty( $openTags ) ) {
					foreach ( $droppedTags as $closingTag ) {
						if ( ! in_array( $closingTag[1], $openTags ) ) {
							array_unshift( $openTags, $closingTag[1] );
						}
					}
				} else {
					foreach ( $droppedTags as $closingTag ) {
						$openTags[] = $closingTag[1];
					}
				}
			}
		}
		$truncate = mb_substr( $truncate, 0, $spacepos );
	}
	$truncate .= $ellipsis;
	if ( $html ) {
		foreach ( $openTags as $tag ) {
			$truncate .= '</' . $tag . '>';
		}
	}

	return $truncate;
}

function append_attr_to_element( &$element, $attr, $value ) {
	if ( $element->hasAttribute( $attr ) ) { //If the element has the specified attribute
		$attrs = explode( ' ', $element->getAttribute( $attr ) ); //Explode existing values
		if ( ! in_array( $value, $attrs ) ) {
			$attrs[] = $value;
		} //Append the new value
		$attrs = array_map( 'trim', array_filter( $attrs ) ); //Clean existing values
		$element->setAttribute( $attr, implode( ' ', $attrs ) ); //Set cleaned attribute
	} else {
		$element->setAttribute( $attr, $value );
	} //Set attribute
}

/**
 *
 * Ricarica in modalità asincrona i file css e, se serve, i file javascript
 *
 * @param string $tag
 *
 * @return string
 */
function acerbo_modify_minified( $tag ) {


	if ( strpos( $tag, 'href=' ) !== false ) {
		preg_match( '/href=[\'"]?([^\'" >]+)/', $tag, $matches );
		$tag = '<script>loadCSS( "' . $matches[1] . '" );</script><noscript><link href="' . $matches[1] . '" rel="stylesheet"></noscript>';
	}
//    if(strpos($tag, 'src=') !== false) {
//    preg_match('/src=[\'"]?([^\'" >]+)/', $tag, $matches);
//    $tag = '<script>loadJS( "'.$matches[1].'" );</script>';
//    }
	return $tag;
}

if ( class_exists( 'BWP_MINIFY' ) ) {
	// add_filter('bwp_minify_get_tag', 'acerbo_modify_minified');
}

//add_filter('script_loader_src', function ( $tag, $handle ) {
//    if (strpos($handle,'jquery') === false) {
//        echo '<script>loadJS( "' . $tag . '" );</script>';
//    } else {
//        return $tag;
//    }
//}, 10, 2);
//
//add_filter('style_loader_src', function ( $tag, $handle ) {
//    if (strpos($handle,'jquery') === false) {
//        echo '<script>loadCSS( "' . $tag . '" );</script>';
//    } else {
//        return $tag;
//    }
//}, 10, 2);

function get_ver() {
	global $developer;
	if ( $developer != 'yes' ) {
		return '001';
	} else {
		return time();
	}
}


/**
 * Filtro il bloginfo
 */
function acerbo_replace_bloginfo( $output, $show ) {
	if ( $show == 'name' ) {
		$site_title = ot_get_option( 'site_title' );
		if ( ! empty( $site_title[0]['title'] ) ) {
			return $site_title[0]['title'];
		} else {
			return $output;
		}
	} elseif ( $show == 'description' ) {
		if ( ! empty( $site_title[0]['title'] ) ) {
			return $site_title[0]['description'];
		} else {
			return $output;
		}
	} else {
		return $output;
	}
}

add_filter( 'bloginfo', 'acerbo_replace_bloginfo', 10, 2 );


if ( ! isset( $content_width ) ) {
	$content_width = 708;
}

/**
 * Filter the Video Embeds
 *
 * This filters Wordpress' built-in embeds and catches the URL if
 * it's one of the whitelisted providers. I'm only supporting YouTube and
 * Vimeo for now, but if demand is high, I might add more.
 *
 * @return string filtered or unfiltered $html
 * @uses $this->is_feed()
 *
 */
function acerbo_video_embed( $html, $url, $attr ) {
	if ( is_feed() ) {
		return $html;
	}

	if ( preg_match( '/(youtu\.?be)/i', $url ) || preg_match( '/(vimeo)/i', $url ) ) {
		$document = new DOMDocument();
		libxml_use_internal_errors( true );
		@$document->loadHTML( $html );

		$lst = $document->getElementsByTagName( 'iframe' );
		for ( $i = 0; $i < $lst->length; $i ++ ) {
			$iframe = $lst->item( $i );
			$iframe->removeAttribute( 'width' );
			$iframe->removeAttribute( 'height' );
			$iframe->removeAttribute( 'frameborder' );
		}
		//save our domdoc back to html
		$html = $document->saveHTML();
		$html = '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
	}

	return $html;
}

add_filter( 'embed_oembed_html', 'acerbo_video_embed', 16, 3 );

// Remove the version number of WP
// Warning - this info is also available in the readme.html file in your root directory - delete this file!
remove_action( 'wp_head', 'wp_generator' );

/*
 * Aggiungo un elemento alla fine e all'inizio del menu1
 */

function acerbo_add_menu_items( $items, $args ) {
	if ( $args->theme_location == 'menu1' ) {
		if ( ! is_front_page() ) {
			$items = '<li id="menu-item-0" class="hidden-xs menu-item menu-item-0 menu-item-hidden"><a href="/" title="Torna all\'homepage"><i class="fa fa-home"></i></a></li>' . $items;
		}
		$items .= '<li id="menu-item-1" class="hidden-xs menu-item menu-item-1 menu-item-hidden"><a data-scroll data-options=\'{"speed": 500,"easing": "easeInOutCubic","offset": 0,"updateURL": false}\' href="#mbAll" title="Torna ad inizio pagina"><i class="fa fa-chevron-circle-up"></i></a></li>';
	}

	return $items;
}

add_filter( 'wp_nav_menu_items', 'acerbo_add_menu_items', 10, 2 );

/**
 *
 * Filtro la voce di menu che contiene un riferimento in page. Se non è in homepage, rindirizzo alla homepage
 *
 * @param type $items Elementi del menu
 *
 * @return type
 */
function acerbo_change_menu( $items ) {
	if ( ! is_front_page() ) {
		foreach ( $items as $item ) {
			$item->url = ( $item->url == '#aggiornamenti' ) ? '/#aggiornamenti' : $item->url;
		}
	}

	return $items;
}

add_filter( 'wp_nav_menu_objects', 'acerbo_change_menu' );


/*
 * OPTION TREE
 */

add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_show_pages', '__return_false' );

/**
 *
 * Minifica l'HTML (JS e CSS inline) attraverso la libreria Minify
 * https://github.com/mrclay/minify
 *
 * @param type $buffer
 *
 * @return type
 */
function acerbo_minify_html( $buffer ) {
	$buffer = Minify_HTML::minify( $buffer, array( 'jsMinifier' => array( 'JSMin\\JSMin', 'minify' ) ) );
//	$buffer = preg_replace( '/https?:\/\/www\.istitutotecnicoacerbope\.(?:edu|gov)\.it\/wp-content\/uploads\/(.*)\.(gif|jpg|jpeg|png)/i', 'https://istitutoacerbo.appspot.com/images/$1.$2', $buffer );

	return $buffer;
}

function acerbo_minify() {
	if ( class_exists( 'Minify_HTML' ) ) {
		ob_start( 'acerbo_minify_html' );
	}
}

add_action( 'template_redirect', 'acerbo_minify' );


/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'theme-options.php' );
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );


/**
 * Aggiungi i sottotitoli alle pagine
 */
add_action( 'init', 'my_add_excerpts_to_pages' );

function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}

/**
 * Aggiunge un'immagine
 */
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'slider', 1280, 400, true );
	add_image_size( 'slider-md', 1022, 367, true );
	add_image_size( 'slider-sm', 768, 336, true );
	add_image_size( 'slider-xs', 375, 220, true );
	add_image_size( 'col3', 262, 147, true ); // (ritagliata)
	add_image_size( 'sticked', 460, 300, true ); // (ritagliata)
	add_image_size( 'showed', 750, 220, true ); // (ritagliata)
	add_image_size( 'mediumslider', 300, 300, true ); // (ritagliata)
	add_image_size( 'icon', 200, 50, true ); // (ritagliata)
	add_image_size( 'iconx2', 400, 100, true ); // (ritagliata)
//    add_image_size('looped', 700, 169, true); // (ritagliata)
//    add_image_size( 'slider-img-lg', 1280, 400, true );
//    add_image_size( 'slider-img-md', 1022, 400, true );
//    add_image_size( 'slider-img-sm', 940, 400, true );
//    add_image_size( 'slider-img-xs', 375, 220, true );
//
//    add_image_size( 'col3-img-lg', 262, 147, false );
//    add_image_size( 'col3-img-md', 212, 119, false );
//    add_image_size( 'col3-img-sm', 344, 193, false );
//    add_image_size( 'col3-img-xs', 158, 88, false );
//
//    add_image_size( 'featured-img-lg', 750, 228, true );
//    add_image_size( 'featured-img-md', 750, 228, true );
//    add_image_size( 'featured-img-sm', 750, 228, true );
//    add_image_size( 'featured-img-xs', 375, 160, true );
}

/**
 *
 * Restituisce un'immagine responsive
 *
 * @param type $id Id del post
 * @param type $size dimensioni
 * @param type $alt Nome Alternativo
 */
function cax_rwd_image( $id, $size ) {
	$lg  = wp_get_attachment_image_src( $id, $size . '-img-lg' );
	$md  = wp_get_attachment_image_src( $id, $size . '-img-md' );
	$sm  = wp_get_attachment_image_src( $id, $size . '-img-sm' );
	$xs  = wp_get_attachment_image_src( $id, $size . '-img-xs' );
	$alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

	return '<picture><!--[if IE 9]><video style="display: none;"><![endif]-->'
	       . '<source srcset="' . $lg['0'] . '" media="(min-width: 1200px)">'
	       . '<source srcset="' . $md['0'] . '" media="(min-width: 992px)">'
	       . '<source srcset="' . $sm['0'] . '" media="(min-width: 768px)">'
	       . '<source srcset="' . $xs['0'] . '"><!--[if IE 9]></video><![endif]-->'
	       . '<img srcset="' . $xs['0'] . '" alt="' . $alt . '"></picture>';
}

function cax_picture_tag() {
	echo '<script>document.createElement( "picture" );</script>';
}

//add_action( 'wp_head', 'cax_picture_tag', 99 );

/**
 *
 * Recupera l'ID dell'immagine a partire dall'url
 *
 * @param type $image_url
 *
 * @return type
 * @global type $wpdb
 *
 */
function acerbo_get_image_id( $image_url ) {
	global $wpdb;
	$prefix     = $wpdb->prefix;
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM " . $prefix . "posts" . " WHERE guid='%s';", $image_url ) );
	$attachment = array_filter( $attachment );
	if ( ! empty( $attachment ) ) {
		return $attachment[0];
	} else {
		return null;
	}
}

/**
 *
 * Funzione has_gallery: verifica se un post ha una galleria di immagini
 *
 * @param type $post_id
 *
 * @return type
 * @global type $post
 *
 */
function has_gallery( $post_id = false ) {
	if ( ! $post_id ) {
		global $post;
	} else {
		$post = get_post( $post_id );
	}

	return ( strpos( $post->post_content, '[gallery' ) !== false );
}

/**
 * Modifico il numero dell'excerpt
 */
function custom_excerpt_length( $length ) {
	return 30;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Creazione dei pulsanti supplementari per l'editor
 */
//add_action('admin_init', 'cax_popup_init');

function cax_popup_init() {
	if ( current_user_can( 'publish_posts' ) ) {
		add_filter( 'mce_buttons', 'cax_filter_mce_button' );
		add_filter( 'mce_external_plugins', 'cax_filter_mce_plugin' );
	}
}

function cax_filter_mce_button( $buttons ) {
	// add a separation before our button, here our button's id is &quot;mygallery_button&quot;
	array_push( $buttons, '|', 'cax_pdf' );

	return $buttons;
}

function cax_filter_mce_plugin( $plugins ) {
	// this plugin file will work the magic of our button
	$plugins['cax_pdf'] = get_stylesheet_directory_uri() . '/inc/tinymce/youtube_editor_plugin.js';

	return $plugins;
}

/** TAG DI MONTEZUMA * */
function bfa_if_front_else( $if, $else ) {
	if ( is_front_page() ) {
		echo $if;
	} else {
		echo $else;
	}
}

function bfa_loop( $postformat = 'postformat' ) {

	if ( have_posts() ) :
		?>

        <div class="post-list">
			<?php
			while ( have_posts() ): the_post();
				get_template_part( $postformat, get_post_format() );
			endwhile;
			?>
        </div>

	<?php else : ?>

        <div id="post-0" class="post not-found">
            <h2>Non abbiamo trovato nulla</h2>

            <div class="post-bodycopy">
                <p>Ci scusiamo, ma questa pagina non contiene nulla. Provi ad usare il motore di ricerca attivabile in
                    testa a questa pagina.</p>
            </div>
        </div>

	<?php
	endif;
}

function bfa_attachment_url( $echo = true ) {
	global $post;
	if ( $echo == true ) {
		echo wp_get_attachment_url( $post->ID );
	} else {
		return wp_get_attachment_url( $post->ID );
	}
}

function bfa_attachment_image( $size, $echo = true ) {
	global $post;
	if ( $echo == true ) {
		echo wp_get_attachment_image( $post->ID, $size );
	} else {
		return wp_get_attachment_image( $post->ID, $size );
	}
}

function bfa_parent_permalink( $echo = true ) {
	global $post;
	if ( $echo == true ) {
		echo get_permalink( $post->post_parent );
	} else {
		return get_permalink( $post->post_parent );
	}
}

function bfa_parent_title( $echo = true ) {
	global $post;
	if ( $echo == true ) {
		echo get_the_title( $post->post_parent );
	} else {
		return get_the_title( $post->post_parent );
	}
}

function bfa_attachment_caption( $before = '<p class="wp-caption-text">', $after = '</p>' ) {
	global $post;
	if ( ! empty( $post->post_excerpt ) ) :
		echo $before;
		// the_excerpt();
		echo $post->post_excerpt;
		echo $after;
	endif;
}

function bfa_image_size() {
	$meta = wp_get_attachment_metadata();
	echo $meta['width'] . '&times;' . $meta['height'];
}

function bfa_image_meta( $args = '' ) {

	$defaults = array(
		'keys'          => '',
		'before'        => '',
		'after'         => '',
		'item_before'   => '',
		'item_after'    => '',
		'item_sep'      => ' &middot; ',
		'key_before'    => '',
		'key_after'     => ': ',
		'value_before'  => '',
		'value_after'   => '',
		'display_empty' => false
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	$meta = wp_get_attachment_metadata();

	$string_array = array();

	// All keys, alphabetically sorted, as provided by wp_get_attachment_metadata()
	if ( $keys == '' ) {
		$array_keys = array_keys( $meta['image_meta'] );
		// Only keys specificed in parameter:
	} else {
		$array_keys = array_map( 'trim', explode( ',', $keys ) );
	}

	foreach ( $array_keys as $key ) {

		$value = $meta['image_meta'][ $key ];

		if ( $display_empty === true || ( $value != '' && $value != '0' ) ) {

			if ( $key == 'created_timestamp' ) // Transform timestamp into readable date, based on default WP date/time settings:
			{
				$value = date( get_option( 'date_format' ) . ' - ' . get_option( 'time_format' ), $value );
			}

			// Prettify key
			$key = ucwords( str_replace( '_', ' ', $key ) );
			$key = $key == 'Iso' ? 'ISO' : $key;


			$key = str_replace(
				array(
					'Aperture',
					'Credit',
					'Camera',
					'Caption',
					'Created Timestamp',
					'Copyright',
					'Focal Length',
					'ISO',
					'Shutter Speed',
					'Title'
				), array(
				__( 'Aperture', 'montezuma' ),
				__( 'Credit', 'montezuma' ),
				__( 'Camera', 'montezuma' ),
				__( 'Caption', 'montezuma' ),
				__( 'Timestamp', 'montezuma' ),
				__( 'Copyright', 'montezuma' ),
				__( 'Focal Length', 'montezuma' ),
				__( 'ISO', 'montezuma' ),
				__( 'Shutter Speed', 'montezuma' ),
				__( 'Title', 'montezuma' )
			), $key
			);


			// Glue it together
			$string = $item_before
			          . $key_before
			          . $key
			          . $key_after
			          . $value_before
			          . $value
			          . $value_after
			          . $item_after;

			$string_array[] = $string;
		}
	}

	$final_string = '';

	// Glue together with item separator
	if ( ! empty( $string_array ) ) {
		$final_string = implode( $item_sep, $string_array );
	}

	// Wrap into parent container
	if ( $final_string != '' ) {
		$final_string = $before . $final_string . $after;
	}

	echo $final_string;
}

// Theme setup
function montezuma_setup() {
	load_theme_textdomain( 'montezuma', get_template_directory() . '/languages' );
	// Add all 9 WP post formats
//	add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video' ) );
	add_theme_support( "post-thumbnails" );
	// set_post_thumbnail_size( 320, 180, true );
	add_theme_support( "automatic-feed-links" );
	register_nav_menus( array( "menu1" => __( "Menu 1", "montezuma" ), "menu2" => __( "Menu 2", "montezuma" ) ) );
}

add_action( 'after_setup_theme', 'montezuma_setup' );

/**
 * SIDEBAR
 */
function acerbo_sidebars_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar Principale', 'acerbo' ),
		'id'            => 'sidebar-1',
		'description'   => 'La sidebar che compare in tutto il sito ad eccezione della homepage',
		'before_widget' => '<aside id="%1$s" class="widget %2$s clearfix">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3><i class="fa fa-caret-right"></i>&nbsp;<span>',
		'after_title'   => '</span></h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar Homepage', 'acerbo' ),
		'id'            => 'sidebar-2',
		'description'   => 'La sidebar che compare solo nella homepage',
		'before_widget' => '<aside id="%1$s" class="widget %2$s clearfix">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3><i class="fa fa-caret-right"></i>&nbsp;<span>',
		'after_title'   => '</span></h3>',
	) );
}

add_action( 'widgets_init', 'acerbo_sidebars_init' );

/**
 * Paginazione stile Bootstrap
 */
if ( function_exists( 'wp_pagenavi' ) ) {
	add_filter( 'wp_pagenavi', 'acerbo_pagination', 10, 2 );

	function acerbo_pagination( $html ) {
		$out = '';

		//wrap a's and span's in li's
		$out = str_replace( "<div", "", $html );
		$out = str_replace( "class='wp-pagenavi'>", "", $out );
		$out = str_replace( "<a", "<li><a", $out );
		$out = str_replace( "</a>", "</a></li>", $out );
		$out = str_replace( "<span", "<li><span", $out );
		$out = str_replace( "<li><span class='pages'>", "<li class='disabled'><span>", $out );
		$out = str_replace( "<li><span class='current'>", "<li class='active'><span>", $out );
		$out = str_replace( "</span>", "</span></li>", $out );
		$out = str_replace( "</div>", "", $out );

		return '<nav>
            <ul class="pagination pagination-centered">' . $out . '</ul>
        </nav>';
	}

}

/**
 * Restituisce un url accorciata con Google Url Shortner. False se errore.
 *
 * @param string $myUrl L'url da accorciare
 *
 * @return bool|mixed
 */
function acerbo_get_short( $id ) {

	if ( empty( $id ) ) {
		return false;
	}

	$short = get_post_meta( $id, 'shortgoo', true );
	if ( ! empty( $short ) ) {
		return $short;
	}

	$headers = [ 'Referer' => 'http://www.istitutotecnicoacerbope.edu.it/' ];
	$myUrl   = get_the_permalink( $id );
	$client  = new Google_Client();
	$client->setApplicationName( "acerbo_short" );
	$client->addScope( "https://www.googleapis.com/auth/urlshortener" );
	$client->setDeveloperKey( "AIzaSyBcx7yGpxQ75sHAKbSY8clxrCenAFWEBOY" );

	$client->setHttpClient( new \GuzzleHttp\Client( array(
		'verify'  => false,
		'headers' => $headers
	) ) );

	$service = new Google_Service_Urlshortener( $client );

	try {
		$url          = new Google_Service_Urlshortener_Url();
		$url->longUrl = $myUrl;
		$short        = $service->url->insert( $url );
		if ( ! empty( $short ) ) {
			update_post_meta( $id, 'shortgoo', $short['id'] );

			return $short['id'];
		}
	} catch ( Google_Service_Exception $e ) {
//	    print_r($e);
		return false;
	}

	return false;
}



