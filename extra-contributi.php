<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 06/13/2018
 * Time: 14:50
 */

$args = array(
	'posts_per_page' => 10,
	'post_type'      => array( 'contributo' )
);

$myquery = get_posts( $args );
$out     = array();


foreach ( $myquery as $post ) {
	$out[] = '<div><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></div>';
}
?>
<div id="docente" class="contributi col-md-12">
    <div class="left">
                <span class="teaser">
                    <a href="<?php echo get_post_type_archive_link( 'contributo' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/mela.png" height="40"
                             width="40" alt="immagine di una mela verde"/>&nbsp;&nbsp;Succede all'Acerbo</a></span>
    </div>
    <div class="owl-contributi owl-carousel right">
		<?php echo implode( '', $out ); ?>
    </div>
</div>
