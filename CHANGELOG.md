<a name"0.4.0"></a>
## 0.4.0 (2015-12-28)


<a name"0.3.2"></a>
### 0.3.2 (2015-11-29)


#### Bug Fixes

* **css:**
  * ritoccato box-shadow ([d0dbad48](http://gitorious.dyndns.org/francesco/acerbostrap/commit/d0dbad48))
  * fix dimensione immagini ([3cc0a4d6](http://gitorious.dyndns.org/francesco/acerbostrap/commit/3cc0a4d6))
  * sistemato loader ajax per modulo CF7 ([e68b2e20](http://gitorious.dyndns.org/francesco/acerbostrap/commit/e68b2e20))
* **git:** eliminato .idea ([49edd387](http://gitorious.dyndns.org/francesco/acerbostrap/commit/49edd387))
* **home:** nuove card ([cdae9474](http://gitorious.dyndns.org/francesco/acerbostrap/commit/cdae9474))
* **homepage:** ritoccate le funzioni della home ([12700831](http://gitorious.dyndns.org/francesco/acerbostrap/commit/12700831))
* **primo piano:** fix ([8cc5336b](http://gitorious.dyndns.org/francesco/acerbostrap/commit/8cc5336b))
* **sito:**
  * allineo git ([646d1c7a](http://gitorious.dyndns.org/francesco/acerbostrap/commit/646d1c7a))
  * completo sito ([36ce3569](http://gitorious.dyndns.org/francesco/acerbostrap/commit/36ce3569))


#### Features

* **avvisi:** aggiunto sistema di gestione degli avvisi via Ajax ([bfb75205](http://gitorious.dyndns.org/francesco/acerbostrap/commit/bfb75205))
* **label:** aggiunta label al primo piano ([f9e3348c](http://gitorious.dyndns.org/francesco/acerbostrap/commit/f9e3348c))
* **primopiano:** nuovo layout del primo piano ([f3290e8a](http://gitorious.dyndns.org/francesco/acerbostrap/commit/f3290e8a))


<a name"0.3.2"></a>
### 0.3.2 (2015-06-16)


#### Features

* **avvisi:** aggiunto sistema di gestione degli avvisi via Ajax ([bfb75205](http://gitorious.dyndns.org/francesco/acerbostrap/commit/bfb75205))



<a name"0.3.1"></a>
### 0.3.1 (2015-04-08)


#### Bug Fixes

* **sticked:**
  * Eliminati elementi sticked dagli archivi di categoria ([bb69c072](http://gitorious.dyndns.org/francesco/acerbostrap/commit/bb69c072))
  * I post sticked si basano sulla data ([e6b184f3](http://gitorious.dyndns.org/francesco/acerbostrap/commit/e6b184f3))


#### Features

* **loop:** Aggiunti elementi sticked agli archivi ([ec92a98b](http://gitorious.dyndns.org/francesco/acerbostrap/commit/ec92a98b))


<a name"0.3.1"></a>
### 0.3.1 (2015-04-08)


#### Bug Fixes

* **sticked:** I post sticked si basano sulla data ([e6b184f3](http://gitorious.dyndns.org/francesco/acerbostrap/commit/e6b184f3))


#### Features

* **loop:** Aggiunti elementi sticked agli archivi ([ec92a98b](http://gitorious.dyndns.org/francesco/acerbostrap/commit/ec92a98b))


<a name"0.2.4"></a>
### 0.2.4 (2015-04-07)


#### Bug Fixes

* **bootstrap:** Aggiornato Bootstrap alla versione 3.3.4 ([4faa2875](http://gitorious.dyndns.org/francesco/acerbostrap/commit/4faa2875))
* **widget:** rimosso span ([70fd8b37](http://gitorious.dyndns.org/francesco/acerbostrap/commit/70fd8b37))


#### Features

* **apuntamenti:** ridotta opacità degli appuntamenti trascorsi ([0c5e8648](http://gitorious.dyndns.org/francesco/acerbostrap/commit/0c5e8648))


<a name"0.2.2"></a>
### 0.2.2 (2015-04-07)


#### Bug Fixes

* **css:** Centrate le immagini in primo piano ([b0e4ec14](http://gitorious.dyndns.org/francesco/acerbostrap/commit/b0e4ec14))


<a name"0.2.1"></a>
### 0.2.1 (2015-04-07)


<a name"0.2.0"></a>
## 0.2.0 (2015-04-06)


#### Bug Fixes

* **grunt:** Corretto changelog ([50583ac6](http://gitorious.dyndns.org/francesco/acerbostrap/commit/50583ac6))


#### Features

* **zaccordion:**
  * Portate a 6 le notizie in primo piano fix(footer): Aggiunto curatore degli aggio ([986d4767](http://gitorious.dyndns.org/francesco/acerbostrap/commit/986d4767))
  * Portate a 6 le notizie in primo piano fix(footer): Aggiunto curatore degli aggio ([60fd29c0](http://gitorious.dyndns.org/francesco/acerbostrap/commit/60fd29c0))


<a name="0.1.2"></a>
### 0.1.2 (2015-03-24)

#### Features

* aggiunto Bump in Grunt ([0b465715](http://gitorious.dyndns.org/francesco/acerbostrap/commit/0b4657156379d32fb4ba8cd2229d0ce14a9c0001))
* Aggiunto changelog ([ca936c67](http://gitorious.dyndns.org/francesco/acerbostrap/commit/ca936c672f4de69629152e225fa5011f1c05a5c2))


<a name="0.1.2"></a>
### 0.1.2 (2015-03-24)


#### Features

* Aggiunto changelog ([ca936c67](http://gitorious.dyndns.org/francesco/acerbostrap/commit/ca936c672f4de69629152e225fa5011f1c05a5c2))
