<?php

/**
 * Funzioni Ajax
 */
//Prende gli eventi
add_action( 'wp_ajax_my_ajax_getevent', 'my_ajax_getevent' );
add_action( 'wp_ajax_nopriv_my_ajax_getevent', 'my_ajax_getevent' );

function my_ajax_getevent() {
	header( 'content-type: application/json; charset=utf-8' );
	$from = $_GET['from'] ? $_GET['from'] : null;
	$to   = $_GET['to'] ? $_GET['to'] : null;

	if ( ! is_numeric( $from ) || ! is_numeric( $to ) ) {
		echo 'nopes';
		exit;
	} else {
		$from = (int) ( $from / 1000 );
		$to   = (int) ( $to / 1000 );
	}

	$args = array(
		'post_status'    => 'publish',
		'post_type'      => array( 'appuntamento' ),
		'posts_per_page' => - 1
	);


	$out   = array();
	$posts = get_posts( $args );
	if ( $posts ) {
		foreach ( $posts as $key => $post ) : setup_postdata( $post );
			$out[ $key ] = array(
				'id'    => $post->ID,
				'title' => $post->post_title,
				'url'   => $post->guid,
				'class' => 'event-' . get_post_meta( $post->ID, 'evento_tipo', true ),
				'start' => get_post_meta( $post->ID, 'evento_unixtime_start', true ) . '000',
				'end'   => get_post_meta( $post->ID, 'evento_unixtime_end', true ) . '000'
			);
			if ( $out[ $key ]['end'] == '000' ) {
				$out[ $key ]['end'] = $out[ $key ]['start'] + 3600000;
			}

		endforeach;
		wp_reset_postdata();
		echo json_encode( array( 'success' => 1, 'result' => $out ) );
	} else {
		echo json_encode( array( 'success' => 1, 'result' => $out ) );
	}
	exit;
}

//Conteggio dei nuovi post
add_action( 'wp_ajax_my_ajax_countpost', 'my_ajax_countpost' );
add_action( 'wp_ajax_nopriv_my_ajax_countpost', 'my_ajax_countpost' );

function my_ajax_countpost() {
	header( 'content-type: application/json; charset=utf-8' );
	$date = $_GET['data'] ? $_GET['data'] : null;

	if ( ! is_numeric( $date ) ) {
		echo 'nopes';
		exit;
	} else {
		$date = (int) $date;
	}

	$args = array(
		'post_status'    => 'publish',
		'post_type'      => array( 'post', 'circolare', 'avviso' ),
		'posts_per_page' => - 1,
		'fields'         => 'ids',
		'date_query'     => array(
			'after' => '@' . $date
		)
	);

	$number_posts = count( get_posts( $args ) );
	echo json_encode( $number_posts );
	wp_reset_postdata();
	exit;
}

// Recupero l'ultimo avviso pubblicato
add_action( 'wp_ajax_my_ajax_readalert', 'my_ajax_readalert' );
add_action( 'wp_ajax_nopriv_my_ajax_readalert', 'my_ajax_readalert' );

function my_ajax_readalert() {
	header( 'content-type: application/json; charset=utf-8' );

	$avviso       = ot_get_option( 'avviso_show', 'no' );
	$testo_avviso = ot_get_option( 'avviso_text', '' );
	$tipo_avviso  = ot_get_option( 'avviso_type', 'warning' );

	$output['show']  = $avviso;
	$output['testo'] = $testo_avviso;
	$output['tipo']  = $tipo_avviso;

	echo json_encode( $output, JSON_PRETTY_PRINT );
	wp_reset_postdata();
	exit;
}


//Conversione dei documenti con le API di Box.net
add_action( 'wp_ajax_my_ajax_convertdoc', 'my_ajax_convertdoc' );
add_action( 'wp_ajax_nopriv_my_ajax_convertdoc', 'my_ajax_convertdoc' );

function my_ajax_convertdoc() {
	header( 'content-type: application/json; charset=utf-8' );
	$id     = $_GET['id'] ? $_GET['id'] : null;
	$result = array();

	if ( ! is_numeric( $id ) ) {
		echo 'nopes';
		exit();
	} else {
		$id = (int) $id;
	}

	require( trailingslashit( get_template_directory() ) . 'inc/box/box-view-api.php' );
	require( trailingslashit( get_template_directory() ) . 'inc/box/box-view-document.php' );

	$api_key = 'qjgzvu2h5pwcdtopm9epcub82owf4qqq';
	$box     = new Box_View_API( $api_key );

	if ( false === ( $value = get_transient( 'box_' . $id ) ) ) {

		$viewDoc = get_post_meta( $id, 'viewid', true );
		if ( ! empty( $viewDoc ) ) {
			$view     = new Box_View_Document();
			$view->id = $viewDoc;
			try {
				$box->view( $view, array( 'is_downloadable' => true, 'duration' => 120 ) );

				$date1    = new DateTime( $view->session->expires_at );
				$date2    = new DateTime( gmdate( "Y-m-d\TH:i:s\Z" ) );
				$viewDiff = ( $date1->getTimestamp() - $date2->getTimestamp() ) - 300;

				$result['result']    = 'ok';
				$result['view']      = $view->session->urls->view;
				$result['assets']    = $view->session->urls->assets;
				$result['realtime']  = $view->session->urls->realtime;
				$result['expire']    = $view->session->expires_at;
				$result['transient'] = $viewDiff;
				$result['type']      = 'cache';
				set_transient( 'box_' . $id, $result, $viewDiff );
				echo json_encode( $result );
				exit();
			} catch ( Exception $e ) {
				$result['result'] = 'ko';
				$result['error']  = $e;
				echo json_encode( $result );
				exit();
			}
		} else {

			$attachment = get_post( $id );
			$src        = wp_get_attachment_url( $id );
			$title      = esc_attr( $attachment->post_title );

			$doc = new Box_View_Document( array(
				'name'     => $title,
				'file_url' => $src
			) );

			try {
				$lol = $box->upload( $doc );
				update_post_meta( $id, 'viewid', $lol->id );
			} catch ( Exception $e ) {
				$result['result']   = 'ko';
				$result['name']     = $title;
				$result['file_url'] = $src;
				$result['error']    = $e->getMessage();
				echo json_encode( $result );
				exit();
			}

			for ( $n = 0; $n < 5; ++ $n ) {
				try {
					$box->view( $doc );

					$date1   = new DateTime( $doc->session->expires_at );
					$date2   = new DateTime( gmdate( "Y-m-d\TH:i:s\Z" ) );
					$docDiff = ( $date1->getTimestamp() - $date2->getTimestamp() ) - 300;

					$result['result']    = 'ok';
					$result['view']      = $doc->session->urls->view;
					$result['assets']    = $doc->session->urls->assets;
					$result['realtime']  = $doc->session->urls->realtime;
					$result['expire']    = $doc->session->expires_at;
					$result['transient'] = $docDiff;
					$result['type']      = 'call';
					set_transient( 'box_' . $id, $result, $docDiff );
					echo json_encode( $result );
					exit();
				} catch ( Exception $e ) {
					if ( $e->getCode() == 202 ) {
						// Apply exponential backoff.
						usleep( ( 1 << $n ) * 1000 + rand( 0, 1000 ) );
					} else {
						$result['result'] = 'ko';
						$result['error']  = $e->getMessage();
						echo json_encode( $result );
						exit();
					}
				}
			}
		}
	} else {
		echo json_encode( $value );
		exit();
	}
}

/**
 * Paginazione infinity
 */
add_action( 'wp_ajax_my_ajax_scroll', 'my_ajax_scroll' );
add_action( 'wp_ajax_nopriv_my_ajax_scroll', 'my_ajax_scroll' );

function my_ajax_scroll() {
	header( 'Content-Type: text/html; charset=utf-8' );
	$expected  = array( 'loop', 'category' );
	$template  = ! empty( $_GET['tmpl'] ) ? sanitize_key( $_GET['tmpl'] ) : null;
	$offset    = ( intval( $_GET['offset'] ) && ! empty( $_GET['offset'] ) ) ? intval( $_GET['offset'] ) : 0;
	$totaleraw = ( intval( $_GET['count'] ) && ! empty( $_GET['count'] ) ) ? intval( $_GET['count'] ) : 5;
	$totale    = ( $totaleraw <= 15 ) ? $totaleraw : 5;
	$cat       = ! empty( $_GET['cat'] ) ? sanitize_key( $_GET['cat'] ) : null;
	$postType  = ! empty( $_GET['pt'] ) ? urldecode( $_GET['pt'] ) : null;
	$key       = ! empty( $_GET['key'] ) ? sanitize_key( $_GET['key'] ) : null;
	$value     = ! empty( $_GET['value'] ) ? sanitize_key( $_GET['value'] ) : null;

	$postTypeArray = explode( ",", $postType );

	if ( ! in_array( $template, $expected ) ) {
		echo 'nope';
		exit;
	} else {
		global $post;
		$args = array(
			'offset'         => $offset,
			'posts_per_page' => $totale,
			'category'       => $cat,
			'post_type'      => $postTypeArray,
			'orderby'        => 'meta_value_num',
			'meta_key'       => 'data_orderby'
		);
		if ( $key && $value ) {
			$args['meta_query'] = array(
				array(
					'key'   => $key,
					'value' => $value
				),
			);
		}

		$scroll_query = get_posts( $args );

		if ( $scroll_query ) {
			foreach ( $scroll_query as $key => $post ) : setup_postdata( $post );
				include( locate_template( 'acerbo-' . $template . '.php' ) );
			endforeach;
			wp_reset_postdata();
		}
		exit;
	}
}
