<?php

/*
 * Rimuovo Contact Form 7
 */
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

// Call the google CDN version of jQuery for the frontend
// Make sure you use this with wp_enqueue_script('jquery'); in your header
function wpfme_jquery_enqueue() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', "", false, null );
	wp_enqueue_script( 'jquery' );
}

if ( ! is_user_logged_in() ) {
	add_action( "wp_enqueue_scripts", "wpfme_jquery_enqueue", 11 );
}

// Call Googles HTML5 Shim, but only for users on old versions of IE
function wpfme_IEhtml5_shim() {
	global $is_IE;
	if ( $is_IE ) {
		echo '<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
	}
}

add_action( 'wp_head', 'wpfme_IEhtml5_shim' );

/* Enqueuing script with IE *version* condition currently not possible 
 * http://core.trac.wordpress.org/ticket/16024
 * I would print this inline into head.php like Tyenty-twelve but doing it like this to avoid theme review issue
 */
add_action( 'wp_head', 'bfa_add_inline_scripts_head' );

function bfa_add_inline_scripts_head() {
	global $is_IE;
	if ( $is_IE ):
		?>
        <!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/javascript/html5.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/javascript/css3-mediaqueries.js"
		        type="text/javascript"></script>
		<![endif]-->
	<?php
	endif;
}

/**
 * Enqueue
 */
function acerbo_scripts() {
	global $developer;
	global $cleaned;
	if ( is_single( 5586 ) || is_page( 5922 ) || is_singular( 'foto_e_video' ) || is_page( 20961 )) {
		wp_enqueue_script( 'jquery-acerbo', get_template_directory_uri() . '/assets/js/jquery-2.1.3.min.js', null, null, false );
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
	}
	if ( $developer == 'yes' ) {

		//Script
		wp_enqueue_script( 'wurfl', 'http://wurfl.io/wurfl.js', null, null, true );
		wp_enqueue_script( 'jquery-acerbo', get_template_directory_uri() . '/assets/js/jquery-2.1.3.min.js', null, null, false );

		// Formstone
		wp_enqueue_script( 'fs-core', get_template_directory_uri() . '/assets/js/lib/dist/formstone/core.js', array( 'jquery-acerbo' ), null, false );
		wp_enqueue_script( 'fs-mediaquery', get_template_directory_uri() . '/assets/js/lib/dist/formstone/mediaquery.js', array( 'fs-core' ), null, false );
		wp_enqueue_script( 'fs-swap', get_template_directory_uri() . '/assets/js/lib/dist/formstone/swap.js', array( 'fs-core' ), null, false );
		wp_enqueue_script( 'fs-touch', get_template_directory_uri() . '/assets/js/lib/dist/formstone/touch.js', array( 'fs-core' ), null, false );
		wp_enqueue_script( 'fs-transition', get_template_directory_uri() . '/assets/js/lib/dist/formstone/transition.js', array( 'fs-core' ), null, false );
		wp_enqueue_script( 'fs-navigation', get_template_directory_uri() . '/assets/js/lib/dist/formstone/navigation.js', array( 'fs-core' ), null, false );
		wp_enqueue_script( 'fs-lightbox', get_template_directory_uri() . '/assets/js/lib/dist/formstone/lightbox.js', array( 'fs-core' ), null, false );

		wp_enqueue_script( 'picturefill', get_template_directory_uri() . '/assets/js/lib/dist/picturefill.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.custom.74916.js', null, null, false );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/lib/dist/bootstrap.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'hoverintent', get_template_directory_uri() . '/assets/js/lib/dist/jquery.hoverIntent.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'lifestream', get_template_directory_uri() . '/assets/js/lib/dist/jquery.lifestream.custom.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'smooth', get_template_directory_uri() . '/assets/js/lib/dist/smooth-scroll.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'hypher', get_template_directory_uri() . '/assets/js/lib/dist/jquery.hypher.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'hypher-it', get_template_directory_uri() . '/assets/js/lib/dist/jquery.hypher.it.js', array( 'hypher' ), null, true );
		wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/assets/js/lib/dist/waypoints.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'waypoints-sticky', get_template_directory_uri() . '/assets/js/lib/dist/waypoints-sticky.js', array( 'waypoints' ), null, true );
		wp_enqueue_script( 'lazyload', get_template_directory_uri() . '/assets/js/lib/dist/lazysizes.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/js/lib/dist/owl.carousel.js', array( 'jquery-acerbo' ), null, true );
		//wp_enqueue_script( 'shifter', get_template_directory_uri() . '/assets/js/lib/dist/jquery.fs.shifter.js', array( 'jquery-acerbo' ), null, true );
		wp_enqueue_script( 'caccavella', get_template_directory_uri() . '/assets/js/custom/dist/acerbo.js', array(
			'modernizr',
			'wurfl',
			'owl',
			'bootstrap',
			'hoverintent'
		), get_ver(), true );
		wp_localize_script( 'caccavella', 'MyAjax', array(
			'ajaxurl'     => admin_url( 'admin-ajax.php' ),
			'count_nonce' => wp_create_nonce( 'ajax_overlay' )
		) );
		wp_enqueue_script( 'ajax-app', get_template_directory_uri() . '/assets/js/custom/dist/ajax-app.js', array( 'caccavella' ), null, true );
		wp_enqueue_script( 'home-app', get_template_directory_uri() . '/assets/js/custom/dist/owl-app.js', array( 'caccavella' ), null, true );
		wp_enqueue_script( 'run-app', get_template_directory_uri() . '/assets/js/custom/dist/run-app.js', array( 'caccavella' ), null, true );

		if ( is_page( 'appuntamenti' ) ) {
			wp_enqueue_script( 'underscore', get_template_directory_uri() . '/assets/js/lib/dist/underscore.js', array( 'jquery-acerbo' ), null, true );
			wp_enqueue_script( 'calendar-lang', get_template_directory_uri() . '/assets/js/lib/dist/calendar.it-IT.js', array( 'jquery-acerbo' ), null, true );
			wp_enqueue_script( 'calendar', get_template_directory_uri() . '/assets/js/lib/dist/calendar.js', array( 'calendar-lang' ), null, true );
			wp_enqueue_script( 'calendar-app', get_template_directory_uri() . '/assets/js/custom/dist/calendar-app.js', array( 'calendar' ), null, true );
		}
		if ( is_single() ) {
			//wp_enqueue_script( 'boxer', get_template_directory_uri() . '/assets/js/lib/dist/jquery.fs.boxer.js', array( 'jquery-acerbo' ), null, true );
		}

		// Stili

		wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array( 'bootstrap' ), null, 'all' );
		wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/fonts.css', array( 'bootstrap' ), null, 'all' );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', null, get_ver(), 'all' );
		wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/fonts.css', array( 'bootstrap' ), null, 'all' );
		wp_enqueue_style( 'owl', get_template_directory_uri() . '/assets/css/owl.carousel.css', array( 'bootstrap' ), null, 'all' );
		//wp_enqueue_style( 'shifter', get_template_directory_uri() . '/assets/css/jquery.fs.shifter.css', array( 'bootstrap' ), null, 'all' );

		wp_enqueue_style( 'fs-navigation', get_template_directory_uri() . '/assets/css/formstone/navigation.css', array( 'bootstrap' ), null, 'all' );
		wp_enqueue_style( 'fs-lightbox', get_template_directory_uri() . '/assets/css/formstone/lightbox.css', array( 'bootstrap' ), null, 'all' );

		if ( is_single() ) {
			if ( $cleaned == 'yes' ) {
				wp_enqueue_style( 'acerbo', get_template_directory_uri() . '/build/css/cleaned.condensed.single.min.css', array( 'bootstrap' ), get_ver(), 'all' );
			}
			//wp_enqueue_style( 'boxer', get_template_directory_uri() . '/assets/css/jquery.fs.boxer.css', array( 'bootstrap' ), null, 'all' );
		}
		if ( is_page( 'appuntamenti' ) ) {
			wp_enqueue_style( 'calendar', get_template_directory_uri() . '/assets/css/calendar.css', array( 'bootstrap' ), null, 'all' );
		}
		if ( is_front_page() ) {
			if ( $cleaned == 'yes' ) {
				wp_enqueue_style( 'acerbo', get_template_directory_uri() . '/build/css/cleaned.condensed.home.min.css', array( 'bootstrap' ), get_ver(), 'all' );
			}
		}
		if ( $cleaned != 'yes' ) {
			wp_enqueue_style( 'acerbo', get_template_directory_uri() . '/assets/css/style.css', array( 'bootstrap' ), get_ver(), 'all' );
		}
		if ( ! is_front_page() && ! is_single() ) {
			if ( $cleaned == 'yes' ) {
				wp_enqueue_style( 'acerbo', get_template_directory_uri() . '/assets/css/style.css', array( 'bootstrap' ), get_ver(), 'all' );
			}
		}
	}
}

if ( ! is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'acerbo_scripts' );
}

/**
 *
 * Aggiunge le formattazioni di bootstrap all'editor di Wordpress
 *
 * @param type $mce_css
 *
 * @return type
 */
function acerbo_mce_css( $mce_css ) {
	if ( ! empty( $mce_css ) ) {
		$mce_css .= ',';
	}
	$mce_css .= get_template_directory_uri() . '/build/admin/bootstrap.editor.condensed.min.css';

	return $mce_css;
}

add_filter( 'mce_css', 'acerbo_mce_css' );
