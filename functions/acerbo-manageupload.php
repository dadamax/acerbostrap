<?php

/**
 * Rinomina il nome del file al momento dell'upload eliminando - e _
 *
 * @param type $attachment_ID
 */
function acerbo_rename_uploaded_file( $attachment_ID ) {

	$post = get_post( $attachment_ID );
	$file = get_attached_file( $attachment_ID );
	$path = pathinfo( $file );
	//dirname   = File Path
	//basename  = Filename.Extension
	//extension = Extension
	//filename  = Filename

	if ( $path['extension'] == 'pdf' ) {
		try {
			// Parse pdf file and build necessary objects.
			$parser   = new \Smalot\PdfParser\Parser();
			$pdf      = $parser->parseFile( $file );
			$text     = $pdf->getText();
			$details  = $pdf->getDetails();
			$filetime = isset( $details['ModDate'] ) ? $details['ModDate'] : null;
			$kea      = new KEA();
			$kea->set_langID( 'it' );
			$tags    = $kea->Autotag( $text );
			$content = implode( ", ", array_keys( $tags ) );
		} catch ( Exception $e ) {
			$content = 'File Acerbo';
		}
	}

	if ( $path['extension'] == 'docx' ) {
		try {
			$docObj = new DocxConversion( $file );
			$text   = $docObj->convertToText();
			$kea    = new KEA();
			$kea->set_langID( 'it' );
			$tags    = $kea->Autotag( $text );
			$content = implode( ", ", array_keys( $tags ) );
		} catch ( Exception $e ) {
			$content = 'File Acerbo';
		}
	}

	$the_post                 = array();
	$the_post['ID']           = $attachment_ID;
	$title                    = str_replace( array( '_', '-' ), ' ', get_the_title( $attachment_ID ) );
	$title                    = ucwords( strtolower( $title ) );
	$the_post['post_title']   = $title;
	$the_post['post_content'] = $content;
	wp_update_post( $the_post );
}

add_action( 'add_attachment', 'acerbo_rename_uploaded_file' );

/**
 * Rimuove altri simboli dai caratteri dei file
 *
 * @param type $query
 *
 * @return type
 */
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array(
		'°',
		'+',
		'’',
		'‘',
		'“',
		'”',
		'«',
		'»',
		'‹',
		'›',
		'—',
		'æ',
		'œ',
		'€',
		'é',
		'à',
		'ç',
		'ä',
		'ö',
		'ü',
		'ï',
		'û',
		'ô',
		'è'
	), $special_chars );

	return $special_chars;
}

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );

/**
 * Aggiunge lo shortcode in allegati di tipo testuale o altro
 *
 */
function acerbo_filter_úpload( $html, $id ) {
	$doc_mime     = array(
		"application/msword",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.template",
		"application/vnd.ms-word.document.macroEnabled.12",
		"application/vnd.ms-word.template.macroEnabled.12"
	);
	$xls_mime     = array(
		"application/vnd.ms-excel",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.template",
		"application/vnd.ms-excel.sheet.macroEnabled.12",
		"application/vnd.ms-excel.template.macroEnabled.12",
		"application/vnd.ms-excel.addin.macroEnabled.12",
		"application/vnd.ms-excel.sheet.binary.macroEnabled.12"
	);
	$ppt_mime     = array(
		"application/vnd.ms-powerpoint",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"application/vnd.openxmlformats-officedocument.presentationml.template",
		"application/vnd.openxmlformats-officedocument.presentationml.slideshow",
		"application/vnd.ms-powerpoint.addin.macroEnabled.12",
		"application/vnd.ms-powerpoint.presentation.macroEnabled.12",
		"application/vnd.ms-powerpoint.slideshow.macroEnabled.12"
	);
	$oo_doc_mime  = array(
		"application/vnd.oasis.opendocument.text",
		"application/vnd.oasis.opendocument.text-template",
		"application/vnd.oasis.opendocument.text-web",
		"application/vnd.oasis.opendocument.text-master"
	);
	$oo_xls_mime  = array(
		"application/vnd.oasis.opendocument.spreadsheet",
		"application/vnd.oasis.opendocument.spreadsheet-template"
	);
	$oo_ppt_mime  = array(
		"application/vnd.oasis.opendocument.presentation",
		"application/vnd.oasis.opendocument.presentation-template"
	);
	$oo_draw_mime = array(
		"application/vnd.oasis.opendocument.graphics",
		"application/vnd.oasis.opendocument.graphics-template"
	);
	$archive_mime = array( "application/zip", "application/x-gzip", "application/rar", "application/7z" );
	$attachment   = get_post( $id );
	$mime_type    = $attachment->post_mime_type;
	$src          = wp_get_attachment_url( $id );
	$title        = esc_attr( $attachment->post_title );

	if ( $mime_type == 'application/pdf' ) {
		$html = '[gpdf url="' . $src . '" title="' . $title . '" doctype="pdf" id="' . $id . '" open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $archive_mime ) ) {
		$html = '[gzip url="' . $src . '" title="' . $title . '" doctype="archive" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $doc_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="word" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $xls_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="excel" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $ppt_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="powerpoint" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $oo_doc_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="word" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $oo_xls_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="excel" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $oo_ppt_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="powerpoint" id="' . $id . '"  open="false"]';

		return $html;
	} elseif ( in_array( $attachment->post_mime_type, $oo_draw_mime ) ) {
		$html = '[gdoc url="' . $src . '" title="' . $title . '" doctype="image" id="' . $id . '"  open="false"]';

		return $html;
	} else {
		return $html;
	}
}

add_filter( 'media_send_to_editor', 'acerbo_filter_úpload', 20, 3 );

/**
 * Shortcode per l'embed dei documenti di tipo PDF
 *
 * @param type $atts
 * @param type $content
 *
 * @return type
 */
function cax_wngpdf( $atts ) {

	$a = shortcode_atts( array(
		'url'     => '',
		'title'   => 'Documento',
		'doctype' => 'pdf',
		'id'      => '',
		'open'    => 'false'
	), $atts );

	if ( preg_match( '/^(https?.*)$/i', $a['url'], $matches ) > 0 ) {
		$content = $matches[1];
	} else {
		$content = '';
	}

	$parse    = parse_url( $content );
	$filePath = $parse['path'];
	$randomId = substr( str_shuffle( "abcdefghijklmnopqrstuvwxyz" ), 0, 6 );

	$title  = '<p class="doctitle"><span class="fa-stack"><i class="fa fa-square fa-stack-2x fa-color-pdf"></i><i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i></span>&nbsp;&nbsp;<a href="' . $content . '" target="_blank">' . str_replace( '_', ' ', $a['title'] ) . '</a><span class="hidden-xs doc__button pull-right"><a href="http://drive.google.com/viewerng/viewer?url=' . $content . '" target="_blank" class="nofancybox"><button type="button" class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="Visualizza in una nuova finestra"><i class="fa fa-external-link"></i></button></a>'
	          . '<a href="' . $content . '" target="_blank" class="nofancybox" title="Scarica il file" download><button type="button" class="btn btn-xs htmldownload"><i class="fa fa-download" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"></i></button></a>'
	          . '<a href="/download.php?dir=' . ltrim( dirname( $filePath ), '/' ) . '&file=' . basename( $filePath ) . '" class="nofancybox" title="Scarica il file"><button type="button" class="btn btn-xs phpdownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a></span></p>';
	$iframe = '<div id="' . $randomId . '" class="docframe embed-responsive embed-responsive-4by3"></div>';
	$nojs   = '<p>Sembra che il tuo browser non possa visualizzare questo Pdf o che non sia abilitato Javascript. Utilizza un <a href="http://browsehappy.com/?locale=it_it">browser migliore</a> oppure <a href="' . $content . '">scarica il Pdf</a></p>';
	$script = '<script>function defer(method) {if (window.$) method();else setTimeout(function() {defer(method)}, 50)}defer(function() {jQuery(document).ready(function($) {$("a.' . $randomId . '").trigger("click")})});</script>';

	if ( $a['open'] == 'true' ) {
		return $title . $iframe . $script;
	} else {
		return $title . $iframe;
	}

}

add_shortcode( 'gpdf', 'cax_wngpdf' );


/**
 * Shortcode per l'embed dei Documenti
 *
 * @param type $atts
 * @param type $content
 *
 * @return type
 */
function cax_wngdoc( $atts ) {

	$a = shortcode_atts( array(
		'url'     => '',
		'title'   => 'Documento',
		'doctype' => 'file',
		'id'      => '',
		'open'    => 'false'
	), $atts );

	if ( preg_match( '/^(https?.*)$/i', $a['url'], $matches ) > 0 ) {
		$content = $matches[1];
	} else {
		$content = '';
	}

	$parse    = parse_url( $content );
	$filePath = $parse['path'];
	$randomId = substr( str_shuffle( "abcdefghijklmnopqrstuvwxyz" ), 0, 6 );

	$title  = '<p class="doctitle"><span class="fa-stack"><i class="fa fa-square fa-stack-2x fa-color-' . $a['doctype'] . '"></i><i class="fa fa-file-' . $a['doctype'] . '-o fa-stack-1x fa-inverse"></i></span>&nbsp;&nbsp;<a href="' . $content . '" target="_blank">' . ucfirst( str_replace( '_', ' ', $a['title'] ) ) . '</a><span class="hidden-xs doc__button pull-right"><a href="http://drive.google.com/viewerng/viewer?url=' . $content . '" target="_blank" class="nofancybox"><button type="button" class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="Visualizza in una nuova pagina"><i class="fa fa-external-link"></i></button></a>'
	          . '<a href="' . $content . '" target="_blank" class="nofancybox" title="Scarica il file" download><button type="button" class="btn btn-xs htmldownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a>'
	          . '<a href="/download.php?dir=' . ltrim( dirname( $filePath ), '/' ) . '&file=' . basename( $filePath ) . '" class="nofancybox" title="Scarica il file"><button type="button" class="btn btn-xs phpdownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a></span></p>';
	$iframe = '<div id="' . $randomId . '" class="docframe embed-responsive embed-responsive-4by3"></div>';

	return $title . $iframe;
}

add_shortcode( 'gdoc', 'cax_wngdoc' );

/**
 * Shortcode per l'embed dei file zip
 *
 * @param type $atts
 * @param type $content
 *
 * @return type
 */
function cax_wngzip( $atts ) {

	$a = shortcode_atts( array(
		'url'     => '',
		'title'   => 'Documento',
		'doctype' => 'archive',
		'id'      => '',
		'open'    => 'false'
	), $atts );

	if ( preg_match( '/^(https?.*)$/i', $a['url'], $matches ) > 0 ) {
		$content = $matches[1];
	} else {
		$content = '';
	}

	$parse    = parse_url( $content );
	$filePath = $parse['path'];

	$title = '<p class="doctitle"><span class="fa-stack"><i class="fa fa-square fa-stack-2x fa-color-' . $a['doctype'] . '"></i><i class="fa fa-file-' . $a['doctype'] . '-o fa-stack-1x fa-inverse"></i></span>&nbsp;&nbsp;<a href="' . $content . '" target="_blank">' . ucfirst( str_replace( '_', ' ', $a['title'] ) ) . '</a><span class="hidden-xs doc__button pull-right"><a href="http://drive.google.com/viewerng/viewer?url=' . $content . '" target="_blank" class="nofancybox"><button type="button" class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="Visualizza in una nuova pagina"><i class="fa fa-external-link"></i></button></a>'
	         . '<a href="' . $content . '" target="_blank" class="nofancybox" title="Scarica il file" download><button type="button" class="btn btn-xs htmldownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a>'
	         . '<a href="/download.php?dir=' . ltrim( dirname( $filePath ), '/' ) . '&file=' . basename( $filePath ) . '" class="nofancybox" title="Scarica il file"><button type="button" class="btn btn-xs phpdownload" data-toggle="tooltip" data-placement="bottom" title="Scarica il file"><i class="fa fa-download"></i></button></a></span></p>';

	return $title;
}

add_shortcode( 'gzip', 'cax_wngzip' );

