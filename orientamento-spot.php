<div class="fullvideo-header-container">
    <div id="slider-bg" class="fullvideo-video-container">
        <video muted
               poster="https://www.istitutotecnicoacerbope.edu.it/wp-content/uploads/2020/12/Spot2020.png?width=1280&height=720&enhance=true"
               id="fullvideo" class="fullvideo-video-playing">
            <source src="http://www.istitutotecnicoacerbope.edu.it/video/spot_2020.webm" type="video/webm">
            <source src="http://www.istitutotecnicoacerbope.edu.it/video/spot_2020.mp4" type="video/mp4">
            Il suo browser non supporta questo video.
        </video>
    </div>
    <div class="fullvideo-page-container">
        <h2 class="hidden-xs">Il video dell'Istituto Tito Acerbo</h2>
        <div class="pull-right fullvideo-video-icon">
             <span id="fullvideo-video-pause" class="fa-stack fa-lg fullvideo-video-pause">
              <i class="fa fa-pause fa-stack-1x"></i>
            </span>
            <span id="fullvideo-video-mute" class="fa-stack fa-lg fullvideo-video-mute">
              <i class="fa fa-volume-up fa-stack-1x"></i>
              <i class="fa fa-ban fa-stack-2x text-primary"></i>
            </span>
            <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.istitutotecnicoacerbope.edu.it%2Fil-video-dellacerbo%2F"
               target="_blank" title="Condividi su Facebook">
                <span class="fa-stack fa-lg fullvideo-video-camera">
                  <i class="fa fa-circle fa-stack-2x fb"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <a href="https://twitter.com/share?url=http%3A%2F%2Fwww.istitutotecnicoacerbope.edu.it%2Fil-video-dellacerbo%2F&via=acerbosocial&text=Il+video+dell%27Istituto+Acerbo"
               target="_blank" title="Condividi su Twitter">
                <span class="fa-stack fa-lg fullvideo-video-camera">
                  <i class="fa fa-circle fa-stack-2x tw"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </div>

        <!--p>La Scuola Superiore più antica di Pescara, a due passi dalla stazione ferroviaria e dal capolinea di
            tutti
            gli autobus urbani ed extraurbani, dal 1923 garantisce a tutti gli studenti provenienti dalla città e
            da tutta la Provincia la qualità della formazione nel rispetto della tradizione.</p-->
    </div>
    <div id="fullvideo-video-close" class="fullvideo-video-close hidden-xs">
        <a href="//www.youtube.com/embed/G2SyMnDRlaM?rel=0" class="boxer-video" title="La Scuola Superiore più antica di Pescara, a due passi dalla stazione ferroviaria e dal capolinea di tutti gli autobus urbani ed extraurbani, dal 1923 garantisce a tutti gli studenti provenienti dalla città e da tutta la Provincia la qualità della formazione nel rispetto della tradizione."><i class="fa fa-expand fa-2x"></i><span class="sr-only">Apri il video a tutto schermo</span></a>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).one('scroll', function () {
            $('#fullvideo')[0].play();
        });
    });
</script>