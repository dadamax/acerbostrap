
<?php

/**
 * Header del sito
 */
get_header();
while ( have_posts() ) : the_post();
	?>
    <style>
        h1, h2, h3, h4 {
            color: #3a5e3b
        }
    </style>
    <h1 class="title" style="background-color: #3a5e3b;color: #fff;"><?php the_title(); ?></h1>
  
    <!-- CONTENUTO CENTRALE -->
    <div id="content" class="cf col-md-8 bd-right">
			<div class="col-md-6">
                  <div class="grid-item">

                   <picture>

                        <!--[if IE 9]>

                        <video

                                style="display: none;"><![endif]-->

                        <source media="(min-width: 992px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">

                        <source media="(min-width: 768px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">

                        <source media="(min-width: 414px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">

                        <!--[if IE 9]></video><![endif]--><img

                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"

                                alt="" class="lazyload img-responsive">

                    </picture>

					 <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
					 	 <h2> Amministrazine Finanza e Marketing </h2>
					 <h3> Smart Lab Dicembre </h3>
                        <ul>

                            <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-dicembre-2020" role="button">Prenota Smart Lab 10 Dicembre</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-dicembre-2020" role="button">Prenota Smart Lab 11 Dicembre</a></p>
                             </li>
							                             <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-dicembre-2020" role="button">Prenota Smart Lab 15 Dicembre</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-dicembre-2020" role="button">Prenota Smart Lab 17 Dicembre</a></p>
                             </li>
							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-dicembre-2020" role="button">Prenota Smart Lab 18 Dicembre</a></p>
                             </li>
                        </ul>

					 					 <h3> Smart Lab Gennaio </h3>
                        <ul>

                            <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 12 Gennaio</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 14 Gennaio</a></p>
                             </li>
							                             <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 15 Gennaio</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 19 Gennaio</a></p>
                             </li>
							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 21 Gennaio</a></p>
                             </li>
							 							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-a-f-m-gennaio-2021/" role="button">Prenota Smart Lab 22 Gennaio</a></p>
                             </li>
                        </ul>
					 
					       
		

					</div>

                </div>
			</div>
  
			<div class="col-md-6">
				<div class="grid-item">

                  <picture>

                        <!--[if IE 9]>

                        <video

                                style="display: none;"><![endif]-->

                        <source media="(min-width: 992px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=262&enhance=true">

                        <source media="(min-width: 768px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=230&width=768&enhance=true">

                        <source media="(min-width: 414px)"

                                data-srcset="https://istitutoacerbo.appspot.com/images/img_afm.png?height=125&width=768&enhance=true">

                        <!--[if IE 9]></video><![endif]--><img

                                src="https://istitutoacerbo.appspot.com/images/img_afm.png?height=177&width=345&enhance=true"

                                alt="" class="lazyload img-responsive">

                    </picture>


					 <div class="grid-paragraph" style="padding-bottom: 20px" data-card="orientamento">
					  	 <h2> Costruzione Ambiente e Territorio </h2>
					 <h3> Smart Lab Dicembre </h3>
                        <ul>

                            <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-dicembre-2020/" role="button">Prenota Smart Lab 10 Dicembre</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-dicembre-2020/" role="button">Prenota Smart Lab 11 Dicembre</a></p>
                             </li>
							                             <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-dicembre-2020/" role="button">Prenota Smart Lab 15 Dicembre</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-dicembre-2020/" role="button">Prenota Smart Lab 17 Dicembre</a></p>
                             </li>
							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-dicembre-2020/" role="button">Prenota Smart Lab 18 Dicembre</a></p>
                             </li>
                        </ul>

					 					 <h3> Smart Lab Gennaio </h3>
                        <ul>

                            <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 12 Gennaio</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 14 Gennaio</a></p>
                             </li>
							                             <li>
						<p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 15 Gennaio</a> </p>
                              </li>  
                            <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 19 Gennaio</a></p>
                             </li>
							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 21 Gennaio</a></p>
                             </li>
							 							                             <li>

                                <p><a class="btn btn-lg btn-success" href="https://www.istitutotecnicoacerbope.edu.it/orientamento-e-open-days/smart-lab/prenotazione-smart-lab-c-a-t-gennaio-2021/" role="button">Prenota Smart Lab 22 Gennaio</a></p>
                             </li>
                        </ul>
					 
					       
		

					</div>
                </div>
	
	
			</div>

        <!-- QUI VA HTML PERSONALIZZATO -->


    <!-- Carosello -->

    <!-- Fine Carosello --> 

    <!-- Libreria -->

      <!-- Fine Libreria-->

		<?php
		/**
		 * Contenuto pubblicato in Wordpress
		 */
		the_content();
		?>



    <div class="col-md-12">           
		 <div class="grid-item">
 <?php echo do_shortcode('[playlist type="video" ids="21224"]'); ?>
 
	</div>
		</div>
    </div>
	
	
	
	
	
	
    <!-- FINE CONTENUTO CENTRALE -->
<?php endwhile; ?>


    <!-- SIDEBAR DESTRA -->
    <div id="widgetarea-one" class="col-md-4 bd-left-minus">

		<?php
		get_sidebar( 'orientamento' );
		?>

    </div>
    <!-- FINE SIDEBAR DESTRA -->


<?php
/**
 * FOOTER del sito
 */
get_footer();
?>