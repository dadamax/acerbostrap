<div id="linkveloci" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">Questa settimana all'Acerbo</h1>
    <?php
    if (function_exists('ot_get_option')) {
        $fastlinkText = ot_get_option('fastlink_text', array());
        if (!empty($fastlinkText)) {
            $doc = new DOMDocument();
            $doc->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">'.$fastlinkText);
            $textareas = $doc->getElementsByTagName('p');
            foreach ($textareas as $textarea) {
                append_attr_to_element($textarea, 'class', 'lead');
            }
            echo $doc->saveHTML();
        }
    }
    ?>
    <div class="row fastlink">
        <?php
        if (function_exists('ot_get_option')) {
            $fastlinks = ot_get_option('fastlink', array());
            if (!empty($fastlinks)) {
                $i = 0;
                foreach ($fastlinks as $fastlink) {
                    if ($i % 4 == 0) {
                        echo '<div class="clearfix"></div>';
                    } elseif ($i % 2 == 0) {
                        echo '<div class="clearfix visible-xs-block visible-sm-block"></div>';
                    }
                    if (!empty($fastlink['link'])) {
                        echo '<div class="col-sm-6 col-md-3"><div class="page-about"><h3 class="grid-paragraph"><a href="' . $fastlink['link'] . '">' . $fastlink['title'] . '</a></h3><p>' . $fastlink['description'] . '</p></div></div>';
                    } else {
                        echo '<div class="col-sm-6 col-md-3"><div class="page-about"><h3 class="grid-paragraph">' . $fastlink['title'] . '</h3><p>' . $fastlink['description'] . '</p></div></div>';
                    }
                    $i++;
                }
            }
        }
        ?>
    </div>
</div>
<div class="clearfix"></div>