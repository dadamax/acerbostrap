<div id="inevidenza" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">In evidenza</h1>
    <div class="row fastlink">

		<?php
		if ( function_exists( 'ot_get_option' ) ) {
			$input    = ot_get_option( 'in_evidenza', array() );
			$features = array_slice( $input, 0, 8 );
			if ( ! empty( $features ) ) {
				$i = 0;
				foreach ( $features as $feature ) {
					if ( $i % 4 == 0 ) {
						echo '<div class="clearfix"></div>';
					} elseif ( $i % 2 == 0 ) {
						echo '<div class="clearfix visible-xs-block visible-sm-block"></div>';
					}
					if ( ! empty( $feature['image'] ) ) {
						$imgId     = acerbo_get_image_id( $feature['image'] );
						$thumb_src = wp_get_attachment_image_src( $imgId, 'col3' );
						echo '<div class="col-xs-12 col-sm-6 col-md-3"><div class="col3-img img-opaque">'
						     . '<a href="' . $feature['link'] . '"><div class="ratio-container inevidenza-container">' . wp_get_attachment_image( $imgId, 'col3', '', array(
								'class'    => "lazyload attachment-col3 center-block img-rounded img-responsive",
								'data-src' => acerbo_cloudinary_img( $thumb_src[0], $thumb_src[1], $thumb_src[2] ),
								'src'      => 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
								'alt'      => trim( strip_tags( get_post_meta( $imgId, '_wp_attachment_image_alt', true ) ) ),
							) ) . '</div></a></div><div class="page-about"><h3><a href="' . $feature['link'] . '">' . $feature['title'] . '</a></h3></div><p>' . $feature['description'] . '</p></div>';
					} else {
						echo '<div class="col-xs-12 col-sm-6 col-md-3"><div class="page-about"><h3><a href="' . $feature['link'] . '">' . $feature['title'] . '</a>
        </h3><p>' . $feature['description'] . '</p></div></div>';
					}
					$i ++;
				}
			}
		}
		?>


    </div>

</div>
<div class="clearfix"></div>
