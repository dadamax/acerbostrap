<?php
if (function_exists('ot_get_option')) {
    if (ot_get_option('slider_show') == 'yes') {
        $swipes = ot_get_option('swipe_slider', array());
        if (!empty($swipes)) {
            ?>

            <div id="slider-bg" class='container page-swipe maxistop visible-xs-block' style="padding-left:0;padding-right:0;">
                <div class='owl-slider owl-carousel'>

                    <?php
                    foreach ($swipes as $swipe) {
//                            print_r($swipe);
                        $link = !empty($swipe['link']) ? '<a href="' . $swipe['link'] . '"><span></span></a>' : '';
                        $imgId = acerbo_get_image_id($swipe['image']);
                        $imgFull = wp_get_attachment_image_src($imgId, 'full');
                        if ($imgFull[1] >= 1200) {
                            echo '<div class="wrap"><div class="slider-bg">'
                            . '<div class="ratio-container slider-container"><picture>'
                            . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
                            . '<source media="(min-width: 1200px)" data-srcset="' . $imgFull[0] . '?width=1280&height=200">'
                            . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=1022&height=200">'
                            . '<source media="(min-width: 768px)" data-srcset="' . $imgFull[0] . '?width=768&height=200">'
                            . '<!--[if IE 9]></video><![endif]-->'
                            . '<img alt="'.trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))).'" src="' . $imgFull[0] . '?width=375&height=220" class="lazyload img-responsive imgid-' . $imgId . '">'
                            . '</picture></div>'
                            . '<div class="swipe-over"><h1>' . $swipe['title'] . '</h1><p>' . $swipe['description'] . '</p></div>'
                            . '</div>' . $link . '</div>';
                        }
                    }
                    ?>
                </div>


            </div>
        <?php
        }
    }
}
?>
            

