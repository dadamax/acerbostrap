<?php
$youtube_id = ot_get_option('yt_iframe');
?>

<div class="row">
    <div class="youtube-home col-md-12">
        <iframe width="980" height="551" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?modestbranding=1&rel=0&theme=light" frameborder="0" allowfullscreen class="center-block"></iframe>
    </div>
</div>