<?php get_header(); ?>    
<div id="content" class="col-md-8 ">
    <div class="archive-bodycopy">

        <div id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>   
            <h1><i class="fa fa-exclamation-triangle" style="color: crimson;"></i>&nbsp; Errore: la pagina richiesta non esiste</h1>
            <p>Questa pagina non esiste. Prova a cercare il contenuto richiesto con il nostro motore di ricerca oppure <a href="<?php echo get_permalink(get_page_by_title('Contatti')); ?>">scrivici</a> per segnalare l'errore.</p>
            <h2>Cerca nel sito</h2>
            <div>                
                <?php get_search_form(); ?>             
            </div>             
        </div>         
    </div>    
</div>
<div id="widgetarea-one" class="col-md-4 bd-left">
    <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
    <?php dynamic_sidebar('sidebar-1'); ?>
</div>
<?php get_footer(); ?>