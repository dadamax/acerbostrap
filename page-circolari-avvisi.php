<?php
/*
  Template Name: Circolari e avvisi
 */

$query00 = new WP_Query(array('numberposts' => 10, 'post_type' => array('avviso', 'circolare')));
$query01 = new WP_Query(array('numberposts' => 10, 'post_type' => array('circolare')));
$query02 = new WP_Query(array('numberposts' => 10, 'post_type' => array('avviso')));

get_header();
?>

<div id="content" class="col-md-8 bd-right">

    <?php while (have_posts()) : the_post(); ?>

        <h1 class="title compensate-bs"><?php the_title(); ?></h1>

        <div class="blocchetto" style="margin-top: 20px">
            
            <div id="tab-container" class="tab-container " role="tabpanel">
                <ul class='nav nav-pills nav-justified' style="width:inherit">
                    <li role="presentation" class="active"><a href="#tutto" aria-controls="tutto" role="tab" data-toggle="tab">Tutti</a></li>
                    <li role="presentation"><a href="#circolari" aria-controls="circolari" role="tab" data-toggle="tab">Circolari</a></li>
                    <li role="presentation"><a href="#avvisi" aria-controls="avvisi" role="tab" data-toggle="tab">Avvisi</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tutto" role="tabpanel" class="data-scroll-append tab-pane fade in active">
                        <?php
                        $mypost = $query00->get_posts();
                        if ($mypost) {
                            foreach ($mypost as $post) : setup_postdata($post);
                                get_template_part('acerbo', 'loop');
                            endforeach;
                            wp_reset_postdata();
                        }
                        ?>

                        <nav>
                            <ul class="pager">
                                <li><a href="<?php echo get_post_type_archive_link('circolare'); ?>/page/2" data-tmpl="loop" class="data-scroll" data-pt="circolare,avviso" data-offset="10" data-position="0" >Visualizza documenti precedenti</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div id="circolari" role="tabpanel" class="data-scroll-append tab-pane fade">
                        <?php
                        $mypost = $query01->get_posts();
                        if ($mypost) {
                            foreach ($mypost as $post) : setup_postdata($post);
                                get_template_part('acerbo', 'loop');
                            endforeach;
                            wp_reset_postdata();
                        }
                        ?>
                        <nav>
                            <ul class="pager">
                                <li><a href="<?php echo get_post_type_archive_link('circolare'); ?>/page/2" data-tmpl="loop" class="data-scroll" data-pt="circolare" data-offset="10">Visualizza circolari precedenti</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div id="avvisi" role="tabpanel" class="data-scroll-append tab-pane fade">
                        <?php
                        $mypost = $query02->get_posts();
                        if ($mypost) {
                            foreach ($mypost as $post) : setup_postdata($post);
                                get_template_part('acerbo', 'loop');
                            endforeach;
                            wp_reset_postdata();
                        }
                        ?>
                        <nav>
                            <ul class="pager">
                                <li><a href="<?php echo get_post_type_archive_link('avviso'); ?>/page/2" data-tmpl="loop" class="data-scroll" data-pt="avviso" data-offset="10">Visualizza avvisi precedenti</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<div id="widgetarea-one" class="col-md-4 bd-left-minus">
    <h1 class="title compensate-bs" style="margin-bottom: 25px">Sezioni</h1>
    <?php get_template_part('sidebar', 'archive'); ?>     
</div>    
<?php get_footer(); ?>

