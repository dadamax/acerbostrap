<?php get_header(); ?>


    <div id="content" class="col-md-8 nopad-right bd-right">
        <h1 class="title compensate-bs">
            Novità
        </h1>
        <div class="archive-bodycopy">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ): the_post();
					get_template_part( 'acerbo', 'loop' );
				endwhile;
			else :
				?>

                <div id="post-0" class="post not-found">
                    <h2><?php _e( 'Nothing Found', 'montezuma' ); ?></h2>
                    <div class="post-bodycopy">
                        <p>Ci dispiace ma questa pagina non contiene articoli. Può provare ad usare il nostro motore di
                            ricerca per cercare il contenuto che le serve.</p>
						<?php get_search_form(); ?>
                    </div>
                </div>

			<?php endif; ?>
        </div>
		<?php if ( function_exists( 'wp_pagenavi' ) ) : ?>
			<?php wp_pagenavi(); ?>
		<?php else : ?>
            <div class="alignleft"><?php next_posts_link( 'Contenuti più vecchi' ); ?></div>
            <div class="alignright"><?php previous_posts_link( 'Contenuti più nuovi' ); ?></div>
		<?php endif; ?>

    </div>

    <div id="widgetarea-one" class="col-md-4 bd-left-minus">
        <h1 class="title compensate-bs" style="margin-bottom: 25px">&nbsp;Sezioni</h1>
		<?php get_template_part( 'sidebar', 'archive' ); ?>
    </div>


<?php get_footer(); ?>