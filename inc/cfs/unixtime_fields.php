<?php

class cfs_unixtime extends cfs_field {

    function __construct() {
        $this->name = 'unixtime';
        $this->label = 'Unixtime';
    }

    function input_head($field = null) {
        $this->load_assets();
        ?>


        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/inc/cfs/bootstrap.min.css'; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/inc/cfs/bootstrap-datetimepicker.css'; ?>" />


        <script type="text/javascript">
        // When the document is ready
            jQuery(document).ready(function($) {

                $('.input-daterange').datepicker({
                    todayBtn: "linked",
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    locale: 'it'
                });

            });
        </script>
        <?php
    }

    function html($field) {
        ?>


        <div class="container">
            <div class="row">
                <div class='col-sm-6'>
                    <div class="form-group">
                        <div class='input-group date datetimepicker' id='unixtime<?php echo $field->id; ?>'>
                            <input type='text' class="form-control <?php echo $field->input_class; ?>" name="<?php echo $field->input_name; ?>" value="<?php echo $field->value; ?>" />
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <script>
                    (function($) {
                        var today = new Date();
                        today.setHours(0, 0, 0, 0);
                        $(function() {
                            $(document).on('cfs/ready', '.cfs_add_field', function() {
                                $('.cfs_unixtime:not(.ready)').init_date();
                            });
                            $('.cfs_unixtime').init_date();
                        });

                        $.fn.init_date = function() {
                            this.each(function() {
                                //$(this).find('input.date').datetime();
                                $(this).find('.datetimepicker').datetimepicker({
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: 'fa fa-arrow-left',
                                        next: 'fa fa-arrow-right',
                                        today: 'fa fa-dot-circle-o',
                                        clear: 'fa fa-dot-trash'
                                    },
                                    locale: 'it',
                                    showClear: true,
                                    showTodayButton: true,
                                    stepping: 5
                                });
                                $(this).addClass('ready');
                            });
                        };
                    })(jQuery);
                </script>


            </div>
        </div>

        <?php
    }

    function load_assets() {
        wp_enqueue_script('moment-js', get_template_directory_uri() . '/inc/cfs/moment.min.js', array('jquery'));
        wp_enqueue_script('moment-js-it', get_template_directory_uri() . '/inc/cfs/moment.locale.it.js', array('jquery'));
        wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/inc/cfs/bootstrap.min.js', array('jquery'));
        wp_enqueue_script('bootstrap-datetimepicker', get_template_directory_uri() . '/inc/cfs/bootstrap-datetimepicker.min.js', array('jquery'));
    }

}
