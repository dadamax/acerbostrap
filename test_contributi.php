<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 06/13/2018
 * Time: 14:50
 */

$args = array(
	'posts_per_page' => 10,
	'post_type'      => 'contributi'
);

$myquery = get_posts( $args );

if ($myquery) :
	$out     = array();
	foreach ( $myquery as $post ) {
		$out[] = '<div><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></div>';
	}
	?>
	<style>
		#docente {
			background-color: #fff;
			margin: 0;
			padding: 13px 10px 0 20px;
		}

		#docente div.owl-item a {
			font-size: 18px;
		}

		div.left {
			float: left;
			width: 25%;
		}

		div.right {
			float: left;
			width: 75%;
		}

		#docente .owl-theme .owl-controls {
			top: 0;
			right: 0;
			left: 85%;
			background-color: white;
		}

		#docente .owl-prev {
			margin: 0 5px 0 0;
		}

		#docente .owl-next {
			margin: 0 0 0 5px;
		}

		#docente .owl-carousel .owl-item {
			overflow: hidden
		}

		#docente a {
			color: #666;
			text-transform: uppercase;
			text-decoration: none;
			font-size: 23px;
			font-family: Yanone Kaffeesatz, Helvetica-Light, Roboto, AdobeBlank, sans-serif;
			line-height: 40px;
			white-space: nowrap;
			overflow: hidden;
		}

		@media only screen and (max-width: 767px) {
			#docente .owl-theme .owl-controls {
				margin-top: 10px;
				position: relative;
				margin-left: auto;
				margin-right: auto;
				left: 2%;
				right: 2%;
				top: 85%;
			}

			div.left {
				float: none;
				width: 100%;
				text-align: center;
				margin-bottom: 10px;
			}

			div.right {
				float: none;
				width: 100%;
			}

			#docente a {
				line-height: 23px;
			}

			.owl-dots {
				display: block;
			}

			.owl-nav {
				display: none;
			}
		}

		@media only screen and (min-width: 768px) {
			#docente .owl-theme .owl-controls {
				left: 75%;
			}

			div.left {
				float: left;
				width: 30%;
			}

			div.right {
				float: left;
				width: 70%;
			}
		}

	</style>

	<div id="docente" class="contributi">
		<div class="left">
                <span class="teaser">
                    <a href="/orientamento-e-open-days/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/mela.png" height="40"
                             width="40" alt="immagine di una mela verde"/>&nbsp;&nbsp;
                        Notizie dai docenti</a></span></div>
		<div class="owl-carousel right">
			<?php echo implode( '', $out ); ?>
		</div>
	</div>
	<script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            center: true,
            dots: true,
            nav: false,
            items: 1,
            navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
            responsive: {
                768: {
                    dots: false,
                    nav: true,
                }
            }

        })

	</script>

<?php endif;