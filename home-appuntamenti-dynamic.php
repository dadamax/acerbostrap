<div id="appuntamenti" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">Appuntamenti all'Acerbo</h1>
	<?php
	if ( function_exists( 'ot_get_option' ) ) {
		$fastlinkText = ot_get_option( 'fastlink_text', array() );
		if ( ! empty( $fastlinkText ) ) {
			$doc = new DOMDocument();
			$doc->loadHTML( '<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $fastlinkText );
			$textareas = $doc->getElementsByTagName( 'p' );
			foreach ( $textareas as $textarea ) {
				append_attr_to_element( $textarea, 'class', 'lead' );
			}
			echo $doc->saveHTML();
		}
	}
	?>
    <div class="row fastlink">
        <div class="owl-appuntamenti owl-carousel clearfix">
			<?php
			$appuntamenti = array(
				'numberposts' => 4,
				'post_type'   => array( 'appuntamento' ),
				'meta_query'  => array( array( 'key' => 'evento_show', 'value' => 1 ) ),
				'orderby'     => 'meta_value_num',
				'meta_key'    => 'evento_unixtime_start'
			);
			$mypost       = get_posts( $appuntamenti );
			if ( $mypost ) {
				$i = 0;
				foreach ( $mypost as $post ) : setup_postdata( $post );
					if ( $i % 2 == 0 ) {
						echo '<div class="clearfix visible-sm-block visible-md-block" style="margin-bottom:10px"></div>';
					}
					$start   = get_post_meta( $post->ID, 'evento_unixtime_start', true ) ? get_post_meta( $post->ID, 'evento_unixtime_start', true ) : 0;
					$end     = get_post_meta( $post->ID, 'evento_unixtime_end', true ) ? get_post_meta( $post->ID, 'evento_unixtime_end', true ) : 0;
					$endData = empty( $end ) ? $start : $end;
					$luogo   = get_post_meta( $post->ID, 'evento_luogo', true ) ? get_post_meta( $post->ID, 'evento_luogo', true ) : 0;
					echo '<div class="col-xs-12 col-sm-6 col-lg-3 appuntamento" data-end="' . $endData . '"><div class="calendar-card"><div class="calendar-inner-card">';
					if ( get_post_meta( $post->ID, 'evento_link', true ) == 1 ) {
						echo '<h3 class="grid-paragraph" data-card="evento"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h3>';
					} else {
						echo '<h3 class="grid-paragraph" data-card="evento">' . get_the_title() . '</h3>';
					}
					$content = get_the_content();
					$content = acerbo_truncate( $content, 248, array(
						'ellipsis' => '...',
						'exact'    => true,
						'html'     => true
					) );
					$content = preg_replace(
						'/"(https?:\/\/www\.istitutotecnicoacerbope\.(?:edu|gov)\.it\/wp-content\/uploads\/(?:.*)\.(?:gif|jpg|jpeg|png))"/i',

						'https://res.cloudinary.com/itcacerbo/image/fetch/w_329,h_150,c_fill/$1', $content );

					echo '<p class="event-content">' . $content . '</p>';

					echo '<hr class="styled" style="margin: 10px 0" />';

					if ( ( $end - $start ) > 86400 ) {
						if ( ( gmdate( "m", $start ) != gmdate( "m", $end ) ) ) {
							$startdate = date_i18n( 'j F', $start );
							$enddate   = date_i18n( 'j F', $end );
							echo '<p class="event-date"><i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate . '</p>';
						} else {
							$startdate = date_i18n( 'j', $start );
							$enddate   = date_i18n( 'j F', $end );
							echo '<p class="event-date"><i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate . '</p>';
						}
					} else {
						if ( ( gmdate( "Hi", $end ) == '0000' ) ) {
							$startdate = date_i18n( 'j F G:i', $start );
							echo '<p class="event-date"><i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . '</p>';
						} else {
							$startdate = date_i18n( 'j F G:i', $start );
							$enddate   = date_i18n( 'G:i', $end );
							echo '<p class="event-date"><i class="fa fa-clock-o fa-fw"></i><span class="sr-only">Ora dell\'evento</span> ' . $startdate . ' - ' . $enddate . '</p>';
						}
					}

					if ( ! empty( $luogo ) ) {
						echo '<p class="event-city"><i class="fa fa-map-marker fa-fw"></i><span class="sr-only">Luogo dell\'evento</span>' . $luogo . '</p>';
					}


					echo '</div></div></div>';
					unset( $start );
					unset( $end );
					unset( $startdate );
					unset( $enddate );
					$i ++;
				endforeach;
				wp_reset_query();
				wp_reset_postdata();
			}
			?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
