/*
 * Variabili
 */
var arrayCrc = [],
	sticky,
	test,
	menuOpen = 0,
	elHeight = $( '.custom-sub', this ).height(),
	parHeight = $( '.menu-item-has-children' ).parent().height();

/*
 * Helper vari
 */

jQuery( document ).ready( function ( a ) {
	jQuery( "body" ).css( "opacity", 1 );
	0 == a( "body" ).css( "opacity" ) && a( "body" ).css( "opacity", 1 );
	a( ".acerbo-entry table" ).addClass( "table table-bordered table-striped" );
	a( ".acerbo-entry table" ).removeAttr( "width bgcolor border" );
	a( ".acerbo-entry table tr" ).removeAttr( "width bgcolor border" );
	a( ".acerbo-entry table td" ).removeAttr( "width bgcolor border" );
	a( "iframe" ).wrap( "<div class='embed-responsive embed-responsive-16by9'></div>" );
	a( ".acerbo-entry table" ).wrap( "<div class='table-responsive'></div>" );
	a( "ul.children, ul.sub-menu" ).parent( "li" ).addClass( "has-sub-menu" );
	a( "ul.menu li.active" ).parents( "li:not(.ancestor)" ).addClass( "ancestor" );
} );

/*
 * Genero una popup
 */

$( 'a' ).filter( function () {
	return this.className.match( /ga-share-(tw|fb|ff|gplus|li|pin)|wn-pop/ );
} ).click( function ( e ) {
	e.preventDefault();
	popup = {
		width: 500,
		height: 350
	};
	popup.top = (
		            screen.height / 2
	            ) - (
		            popup.height / 2
	            );
	popup.left = (
		             screen.width / 2
	             ) - (
		             popup.width / 2
	             );
	window.open( $( this ).attr( 'href' ), 'targetWindow', "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=" + popup.left + ",top=" + popup.top + ",width=" + popup.width + ",height=" + popup.height );
} );

/*
 * Disabilito e abilito i console.log
 */
if ( window.location.hostname != 'test.acerbo.it' ) {
	if ( ! window.console ) {
		window.console = {};
	}
	var methods = ["log"];
	for ( var i = 0; i < methods.length; i ++ ) {
		console[methods[i]] = function () {
		};
	}
} else {
	test = true;
}

/*
 * Creazione dei chcksum 'var crctext = crc32(text);'
 */
var makeCRCTable = function () {
	var c;
	var crcTable = [];
	for ( var n = 0; n < 256; n ++ ) {
		c = n;
		for ( var k = 0; k < 8; k ++ ) {
			c = (
				(
					c & 1
				) ? (
					0xEDB88320 ^ (
						c >>> 1
					)
				) : (
					c >>> 1
				)
			);
		}
		crcTable[n] = c;
	}
	return crcTable;
};

var crc32 = function ( str ) {
	var crcTable = window.crcTable || (
		window.crcTable = makeCRCTable()
	);
	var crc = 0 ^ (
		- 1
	);

	for ( var i = 0; i < str.length; i ++ ) {
		crc = (
			      crc >>> 8
		      ) ^ crcTable[(
			                   crc ^ str.charCodeAt( i )
		                   ) & 0xFF];
	}

	return (
		       crc ^ (
			       - 1
		       )
	       ) >>> 0;
};

/**
 * Funzione per l'attesa
 * @param test function that returns a value
 * @param expectedValue the value of the test function we are waiting for
 * @param msec delay between the calls to test
 * @param count (debug) used to count the loops
 * @param source (debug) a string to specify an ID, a message, etc
 * @param callback function to execute when the condition is met
 *
 * Esempio: waitfor(_isBusy, false, 50, 0, 'play->busy false', function() {
 *   alert('The show can resume !');
 *  });
 *
 */
function waitfor( test, expectedValue, msec, count, source, callback ) {
	// Check if condition met. If not, re-check later (msec).
	while ( test() !== expectedValue ) {
		count ++;
		if ( count == 100 ) {
			console.log( 'waitfor count: end at ' + count );
			return
		}
		setTimeout( function () {
			waitfor( test, expectedValue, msec, count, source, callback );
		}, msec );
		console.log( 'waitfor count: ' + count );
		return;
	}
	// Condition finally met. callback() can be executed.
	console.log( source + ': ' + test() + ', expected: ' + expectedValue + ', ' + count + ' loops.' );
	callback();
}

/*
 * Libreria per i cookie
 */

/*\
 |*|
 |*|  :: cookies.js ::
 |*|
 |*|  A complete cookies reader/writer framework with full unicode support.
 |*|
 |*|  Revision #1 - September 4, 2014
 |*|
 |*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
 |*|  https://developer.mozilla.org/User:fusionchess
 |*|
 |*|  This framework is released under the GNU Public License, version 3 or later.
 |*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
 |*|
 |*|  Syntaxes:
 |*|
 |*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
 |*|  * docCookies.getItem(name)
 |*|  * docCookies.removeItem(name[, path[, domain]])
 |*|  * docCookies.hasItem(name)
 |*|  * docCookies.keys()
 |*|
 \*/

var docCookies = {
	getItem: function ( sKey ) {
		if ( ! sKey ) {
			return null;
		}
		return decodeURIComponent( document.cookie.replace( new RegExp( "(?:(?:^|.*;)\\s*" + encodeURIComponent( sKey ).replace( /[\-\.\+\*]/g, "\\$&" ) + "\\s*\\=\\s*([^;]*).*$)|^.*$" ), "$1" ) ) || null;
	},
	setItem: function ( sKey, sValue, vEnd, sPath, sDomain, bSecure ) {
		if ( ! sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test( sKey ) ) {
			return false;
		}
		var sExpires = "";
		if ( vEnd ) {
			switch ( vEnd.constructor ) {
				case Number:
					sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
					break;
				case String:
					sExpires = "; expires=" + vEnd;
					break;
				case Date:
					sExpires = "; expires=" + vEnd.toUTCString();
					break;
			}
		}
		document.cookie = encodeURIComponent( sKey ) + "=" + encodeURIComponent( sValue ) + sExpires + (
			sDomain ? "; domain=" + sDomain : ""
		) + (
			                  sPath ? "; path=" + sPath : ""
		                  ) + (
			                  bSecure ? "; secure" : ""
		                  );
		return true;
	},
	removeItem: function ( sKey, sPath, sDomain ) {
		if ( ! this.hasItem( sKey ) ) {
			return false;
		}
		document.cookie = encodeURIComponent( sKey ) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (
			sDomain ? "; domain=" + sDomain : ""
		) + (
			                  sPath ? "; path=" + sPath : ""
		                  );
		return true;
	},
	hasItem: function ( sKey ) {
		if ( ! sKey ) {
			return false;
		}
		return (
			new RegExp( "(?:^|;\\s*)" + encodeURIComponent( sKey ).replace( /[\-\.\+\*]/g, "\\$&" ) + "\\s*\\=" )
		).test( document.cookie );
	},
	keys: function () {
		var aKeys = document.cookie.replace( /((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "" ).split( /\s*(?:\=[^;]*)?;\s*/ );
		for ( var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx ++ ) {
			aKeys[nIdx] = decodeURIComponent( aKeys[nIdx] );
		}
		return aKeys;
	}
};


/*
 * Configuro Lazysizes
 */

window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.expand = 20;


/**
 * Modulo per gli avvisi
 */

var checkAvviso = (
	function () {

		var nocache = Math.ceil( (
			                         Math.round( new Date().getTime() / 1000 ) + 1
		                         ) / 900 ) * 900,
			avviso_open = '<div class="row cf"><div class="col-sm-12">',
			avviso_tipo_open = '<div class="alert alert-dismissible fade in" role="alert">',
			avviso_button = '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi avviso</span></button>',
			avviso_tipo_close = '</div>',
			avviso_close = '</div></div>';

		var refreshSticky = function () {
			console.log( 'refresh stiky' );
			var el = $( '#main-menu' );
			if ( el.parent( '.sticky-wrapper' ).length ) {
				setTimeout( function () {
					el.waypoint( 'sticky' );
				}, 600 );
			}
		};

		var getAvviso = function () {
			$.getJSON( MyAjax.ajaxurl, {
					action: "my_ajax_readalert",
					id: nocache
				}, function ( data ) {
					if ( data.show == 'yes' && data.testo !== '' ) {
						var avviso_class = 'alert-' + data.tipo,
							avviso_testo = data.testo,
							avviso_testo_crc = crc32( avviso_testo ),
							avviso_tipo_open = '<div class="alert ' + avviso_class + ' alert-dismissible fade in" role="alert">',
							avviso = avviso_open + avviso_tipo_open + avviso_button + data.testo + avviso_tipo_close + avviso_close;

						if ( (
							docCookies.getItem( 'avviso' ) != avviso_testo_crc
						) ) {
							$( '#avviso' ).removeAttr( 'style' );
							$( avviso ).hide().appendTo( '#avviso' ).fadeIn();
							$( '#avviso .alert' ).on( 'closed.bs.alert', function () {
								docCookies.setItem( 'avviso', avviso_testo_crc, 259200, '/' );
								refreshSticky();
							} );
							refreshSticky();
							console.log( 'mostro avviso' );
						} else {
							$( avviso ).appendTo( '#avviso' );
							$( '<div class="avviso_badge" ><span>Avviso</span></div>' ).appendTo( '#avviso' );
							var badge = $( '#avviso .avviso_badge' ),
								parent_badge = badge.parent();
							badge.on( "click", function ( e ) {
								$( '#avviso' ).removeAttr( 'style' );
								refreshSticky();
								badge.detach();
							} );

							$( '#avviso .alert' ).on( 'closed.bs.alert', function () {
								docCookies.setItem( 'avviso', avviso_testo_crc, 259200, '/' );
								refreshSticky();
							} );
							console.log( 'mostro badge' );
						}

					}
					else {
						console.log( 'no avviso' );
					}
				}
			)
			 .error( function () {
				 console.log( 'errore' );
			 } );
		};

		return {
			// A public function utilizing privates
			getCheckAvviso: function () {
				getAvviso();
			}
		};

	}
)
();

/*
 * Avvisi
 */

checkAvviso.getCheckAvviso();

jQuery( document ).ready( function ( $ ) {
	dataLayer.push( {
		'timingCategory': 'Dipendenze',
		'timingVar': 'Full DOM Load',
		'timingValue': performance.now(),
		'timingLabel': 'DOM Load',
		'event': 'timeTrack'
	} );


	/**
	 * Cookie Banner
	 */


	var opts = {
		expires: 'expires_callback',
		cookie: 'acerbo_cb',
		linkmsg: '',
		message: 'Questo sito fa uso solo di cookie tecnici e non utilizza cookie profilanti. I cookie del servizio di Web Analytics (Google Analytics) sono configurati con l\'opzione di anonimato attiva e dunque non raccolgono dati personali.',
		effect: 'fade',
		'accept-on-scroll': true,
		'accept-on-click': true
	};
	if ( typeof Cookiebanner !== 'undefined' ) {
		var banner = new Cookiebanner( opts );
		banner.run();
	}

	/**
	 * Condivisione Whatsapp
	 */

	$( '#mbCont' ).on( 'click', '.whatsapp-share', function ( e ) {
		e.preventDefault();
		var protocol = 'https://api.whatsapp.com/send?text=';
		if ( $( 'body' ).hasClass( 'single' ) ) {
			var link = $( 'meta[property="ac:shorturl"]' ).attr( 'content' ) ? $( 'meta[property="ac:shorturl"]' ).attr( 'content' ) : [
				location.protocol,
				'//',
				location.host,
				location.pathname
			].join( '' );
			var title = $( 'meta[property="og:title"]' ).attr( 'content' ) ? $( 'meta[property="og:title"]' ).attr( 'content' ) : document.title;
		} else {
			var link = $( this ).attr( 'data-shorturl' ) ? $( this ).attr( 'data-shorturl' ) : [
				location.protocol,
				'//',
				location.host,
				location.pathname
			].join( '' );
			var title = $( this ).attr( 'data-title' );
		}
		var message = protocol + 'Leggi questa notizia sul sito dell\'Acerbo: ' + title + '%0A%0D' + encodeURIComponent( link ).replace( /[!'()]/g, escape ).replace( /\*/g, "%2A" ).replace( /%20%0A%20%0A/g, "%20%0A%0D" );
		//console.log(message);
		window.open( message, '_self' );
	} );

	/*
	 * Avviso per le ricerche
	 */

	$( ".searchform" ).submit( function ( e ) {
		if ( $( ".form-control, .navbar-search", this ).val().length === 0 ) {
			$( "<div class='alert alert-warning alert-dismissable' style='margin-top:10px'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Devi scrivere almeno una parola nel modulo per poter eseguire una ricerca.</div>" ).appendTo( this ).hide().fadeIn( 300 ).delay( 5000 ).fadeOut( 300 );
			e.preventDefault();
		}
	} );

	$( ".navbar-search" ).submit( function ( e ) {
		if ( $( ".form-control", this ).val().length === 0 ) {
			$( '.navbar-search .form-control' ).popover( 'show' );
			e.preventDefault();
		}
	} );

	if ( window.chrome ) {
		$( "#slider-home li" ).css( "background-size", "100% 100%" );
	}
} );