/*
 * Chiamate Ajax per vari task
 */


jQuery("a.data-scroll").on("click", function (e) {
    e.preventDefault();
    scrollAppend = $(this).closest('.data-scroll-append');
    scrollButton = $(this);
    scrollNav = $(this).closest('nav');
    scrollEnd = '<div class="panel panel-default "><div class="panel-body text-center">Non ci sono altri contenuti da visualizzare</div></div>';
    $(' <i class="fa fa-spinner fa-spin" style="margin-left: 10px"></i>').hide().appendTo(scrollButton).fadeIn();

    var tmpl = jQuery(this).data('tmpl') ? jQuery(this).data('tmpl') : 'loop';
    var count = jQuery(this).data('count') ? jQuery(this).data('count') : 10;
    var offset = jQuery(this).data('offset') ? jQuery(this).data('offset') : 0;
    var pt = jQuery(this).data('pt') ? jQuery(this).data('pt') : 'post,circolare,avviso';
    var cat = jQuery(this).data('cat') ? jQuery(this).data('cat') : null;
    var key = jQuery(this).data('key') ? jQuery(this).data('key') : null;
    var value = jQuery(this).data('value') ? jQuery(this).data('value') : null;

    console.log(offset);

    jQuery.get(MyAjax.ajaxurl, {
        action: "my_ajax_scroll",
        tmpl: tmpl,
        count: count,
        offset: offset,
        pt: pt,
        cat: cat,
        key: key,
        value: value
    }, function (data) {
        if (data.length != 0) {
            scrollButton.fadeOut(300, function () {
                scrollButton.data('offset', (offset + count));
                $('<hr class="styled">').appendTo(scrollAppend);
                scrollNav.detach();
                $('.fa-spinner', scrollNav).remove();
                $(data).appendTo(scrollAppend).fadeIn();
                scrollAppend.append(scrollNav).fadeIn();
                scrollButton.fadeIn();
            });

        } else {
            scrollButton.fadeOut(300, function () {
                scrollNav.remove();
                scrollAppend.append(scrollEnd).fadeIn();
            });
        }
    });
});

/*
*	Mostra i documenti in un iframe
*/

$("a.getbox").on("click", function (e) {
    e.preventDefault();
    var el = $(this);
    var id = el.data("fileid");
    var frame = el.data("target");
    if (el.data("status") != 'open' && el.data("loaded") != 'true') {
        $('<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 5%"><span class="sr-only">Stiamo completando la richiesta</span></div></div>').appendTo($('#' + frame + ''));
        $('#' + frame + '').fadeIn(function () {
            el.data("status", "open");
        });
        $('#' + frame + ' .progress-bar').width('100%');
        $.getJSON(MyAjax.ajaxurl, {
            action: "my_ajax_convertdoc",
            id: id
        }, function (data) {
            if (data.result == 'ok') {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<iframe src="' + data.view + '"></iframe>').hide().appendTo('#' + frame + '').fadeIn();
                    el.data("loaded", "true");
                    el.closest("h3").addClass('open');
                });
            } else {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<div class="alert alert-warning" role="alert"><strong>Attenzione</strong> Abbiamo riscontrato un errore. Prova a ricaricare la pagina ed eseguire di nuovo la richiesta.</div>').hide().appendTo($('#' + frame + '')).fadeIn();
                    dataLayer.push({
                        'filename': data.name,
                        'fileurl': data.file_url,
                        'error': data.error,
                        'event': 'ajaxError'
                    });
                    el.data("loaded", "false");
                    el.closest("h3").removeClass('open');
                });
            }
        })
            .error(function () {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<div class="alert alert-warning" role="alert"><strong>Attenzione</strong> Abbiamo riscontrato un errore nel recuperare il file. Prova a ricaricare la pagina ed eseguire di nuovo la richiesta.</div>').hide().appendTo($('#' + frame + '')).fadeIn();
                    dataLayer.push({
                        'filename': data.name,
                        'fileurl': data.file_url,
                        'error': data.error,
                        'event': 'ajaxError'
                    });
                    el.data("loaded", "false");
                });
            });
    } else if (el.data("status") != 'open' && el.data("loaded") == 'true') {
        $('#' + frame + '').fadeIn(function () {
            el.data("status", "open");
            el.closest("h3").addClass('open');
        });

    } else if (el.data("status") == 'open' && el.data("loaded") == 'true') {
        $('#' + frame + '').fadeOut(function () {
            el.data("status", "closed");
            el.closest("h3").removeClass('open');
        });
    } else if (el.data("status") == 'open' && el.data("loaded") != 'true') {
        $('#' + frame + '').html('<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 5%"><span class="sr-only">Stiamo completando la richiesta</span></div></div>');
        $('#' + frame + ' .progress-bar').width('100%');
        $.getJSON(MyAjax.ajaxurl, {
            action: "my_ajax_convertdoc",
            id: id
        }, function (data) {
            if (data.result == 'ok') {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<iframe src="' + data.view + '"></iframe>').hide().appendTo('#' + frame + '').fadeIn();
                    el.data("loaded", "true");
                    el.closest("h3").addClass('open');
                });
            } else {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<div class="alert alert-warning" role="alert"><strong>Attenzione</strong> Abbiamo riscontrato un errore. Prova a ricaricare la pagina ed eseguire di nuovo la richiesta.</div>').hide().appendTo($('#' + frame + '')).fadeIn();
                    dataLayer.push({
                        'filename': data.name,
                        'fileurl': data.file_url,
                        'error': data.error,
                        'event': 'ajaxError'
                    });
                    el.data("loaded", "false");
                    el.closest("h3").removeClass('open');
                });
            }
        })
            .error(function () {
                $('#' + frame + ' .progress').fadeOut(function () {
                    $('<div class="alert alert-warning" role="alert"><strong>Attenzione</strong> Abbiamo riscontrato un errore. Prova a ricaricare la pagina ed eseguire di nuovo la richiesta.</div>').hide().appendTo($('#' + frame + '')).fadeIn();
                    dataLayer.push({
                        'filename': data.name,
                        'fileurl': data.file_url,
                        'error': data.error,
                        'event': 'ajaxError'
                    });
                    el.data("loaded", "false");
                    el.closest("h3").removeClass('open');
                });
            });
    }
    // $.get Convertdoc
});
