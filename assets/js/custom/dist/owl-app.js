/*
 * OwlCarousel
 */

var owlAppuntamenti;
var ie8 = navigator.userAgent.match(/Trident\/4\./) ? true : false;

/**
 * Creo lo slider della sezione appuntamenti solo in iPhone
 */


if ((ie8 === false) && ($(".owl-appuntamenti").length > 0)) {
    $.mediaquery("bind", "mq-key", "screen and (max-width: 767px)", {
        enter: function () {
            console.log('enter owl screen and (max-width: 767px)');
            owlAppuntamenti = $('.owl-appuntamenti');
            owlAppuntamenti.on('initialized.owl.carousel', function (e) {
                $('#linkveloci .ajax-loader').remove();
            });
            owlAppuntamenti.owlCarousel({
                items: 1,
                nav: false,
                dots: true,
                dotsEach: true,
                navText: [
                    '<i class="fa fa-chevron-circle-left"></i> Indietro',
                    'Avanti <i class="fa fa-chevron-circle-right"></i>'
                ],
                responsiveClass: false
            });
        },
        leave: function () {
            owlAppuntamenti = $('.owl-appuntamenti');
            owlAppuntamenti.trigger('destroy.owl.carousel');
        }
    });
}


if ($(".owl-partner").length > 0) {
    var owlPartner = $('.owl-partner');

    owlPartner.owlCarousel({
        loop: true,
        margin: 15,
        dots: false,
        nav: false,
        stagePadding: 2,
        navText: [
            '<i class="fa fa-chevron-circle-left"></i> Indietro',
            'Avanti <i class="fa fa-chevron-circle-right"></i>'
        ],
        responsiveClass: true,
        responsive: {
            1: {
                items: 1,
                nav: false,
                dots: true
            },
            768: {
                items: 3,
                nav: false,
                dots: true
            },
            992: {
                items: 4,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true
            },
            1200: {
                items: 5,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true
            }
        }
    });
}

if ($(".owl-certificazioni").length > 0) {
    var owlCertificazioni = $('.owl-certificazioni');

    owlCertificazioni.owlCarousel({
        loop: true,
        dots: false,
        nav: true,
        navText: [
            '<i class="fa fa-chevron-circle-left"></i> Indietro',
            'Avanti <i class="fa fa-chevron-circle-right"></i>'
        ],
        responsiveClass: true,
        responsive: {
            1: {
                items: 1,
                nav: false,
                dots: true
            },
            992: {
                items: 2,
                nav: true
            },
            1200: {
                items: 3,
                nav: true
            }
        }
    });
}


if (($(".contributi").length > 0)) {
    $('.owl-contributi').owlCarousel({
        loop: true,
        margin: 10,
        center: true,
        dots: true,
        nav: false,
        items: 1,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            768: {
                dots: false,
                nav: true
            }
        }
    })
}

if ($(".page-swipe").length > 0) {
    var time = 7, // tempo in secondi per completare la progress
        owl = $('.owl-slider'),
        isPause = false,
        tick,
        percentTime;

    owl.on('initialized.owl.carousel', function (e) {
        $('<div class="progress"><div class="progress-bar" style="width: 0%;"></div></div>').prependTo(e.target);
        console.log(e);
        start();
        jQuery('#slider-bg').css('background-image', 'url()');
    });
    owl.on('translated.owl.carousel', function (e) {
        clearTimeout(tick);
        if (e.item.index != (
                e.item.count - 1
            )) {
            start();
        }
    });
    owl.on('dragged.owl.carousel click', function (e) {
        isPause = true;
        $('.progress .progress-bar').css({
            width: "100%"
        });
    });

    owl.owlCarousel({
        loop: false,
        items: 1
    });

    function start() {
        percentTime = 0;
        tick = setInterval(interval, 5);
    }

    function interval() {
        if (isPause === false) {
            percentTime += 1 / time;
            $('.progress .progress-bar').css({
                width: percentTime + "%"
            });
            if (percentTime >= 100) {
                owl.trigger('next.owl.carousel');
                clearTimeout(tick);
            }
        }
    }

}

