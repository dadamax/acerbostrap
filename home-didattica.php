<div id="sliderpartner" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">Didattica all'Acerbo</h1>

	<?php
	/**
	 * Created by PhpStorm.
	 * User: francesco
	 * Date: 06/13/2018
	 * Time: 14:50
	 */

	$args = array(
		'posts_per_page' => 10,
		'post_type'      => array( 'contributo' )
	);

	$myquery = get_posts( $args );
	$out     = array();


	foreach ( $myquery as $post ) {
		$out[] = '<div><a href="' . get_the_permalink() . '">' . get_the_title() . ', <span>di <em>' . get_the_author_meta( "display_name", $post->post_author ) . '</em></span></a></div>';
	}
	?>
    <div id="docente" class="contributi col-md-12">
        <div class="owl-contributi owl-carousel">
			<?php echo implode( '', $out ); ?>
        </div>
    </div>

</div>

<div class="clearfix"></div>