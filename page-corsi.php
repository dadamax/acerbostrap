<?php
/*
  Template Name: Corsi di studio
 */
get_header();
$shortoutput = '';
?>

<div id="content" class="col-md-12">


    <div id="post-<?php the_ID(); ?>" <?php post_class('acerbo-entry'); ?>>
        <?php
        if (has_post_thumbnail()) {
            echo '<div class="post-thumb compensate-bs"><div class="thumb-inner">';
            the_post_thumbnail('showed');
            echo '</div></div>';
        }
        ?>
        <div class="page-header">
            <h1 class="title compensate-bs">
                <?php the_title(); ?>
            </h1>
        </div>

        <div class="post-bodycopy">
        <p>L'Istituto Tecnico Statale Tito Acerbo, la più antica scuola superiore della provincia di Pescara, fondata
            nel 1923, garantisce, oggi, ai suoi studenti una formazione completa e aggiornata negli studi di economia,
            marketing, informatica, internazionalizzazione dei mercati, turismo, costruzioni ambiente territorio.</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="grid-item">
                    <?php
                    $imgId = 6234;
                    $imgFull = wp_get_attachment_image_src($imgId, 'full');
                    echo '<a href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/amministrazione-finanza-e-marketing"><picture>'
                        . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
                        . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=360&height=200&enhance=true">'
                        . '<source media="(min-width: 415px)" data-srcset="' . $imgFull[0] . '?width=777&height=186&enhance=true">'
                        . '<!--[if IE 9]></video><![endif]-->'
                        . '<img src="' . $imgFull[0] . '?width=414&height=177&enhance=true" alt="' . trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))) . '" class="lazyload img-responsive">'
                        . '</picture></a>';
                    ?>
                    <div class="grid-paragraph" data-card="corsi">
                        <h3>Amministrazione, Finanza e Marketing</h3>
                        <p>Le discipline di studio caratterizzanti l'indirizzo Amministrazione,
                            Finanza e Marketing sono l'economia aziendale, il diritto, l'informatica, 4 lingue
                            straniere, le relazioni
                            internazionali per il marketing.</p>


                    </div>
                    <div style="padding: 0 10px 10px 10px">
                    <p align="center"><a
                            href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/amministrazione-finanza-e-marketing"
                            class="btn btn-info btn-block">Tutti i dettagli</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid-item">
                    <?php
                    $imgId = 6233;
                    $imgFull = wp_get_attachment_image_src($imgId, 'full');
                    echo '<a href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/costruzioni-ambiente-e-territorio"><picture>'
                        . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
                        . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=360&height=200&enhance=true">'
                        . '<source media="(min-width: 415px)" data-srcset="' . $imgFull[0] . '?width=777&height=186&enhance=true">'
                        . '<!--[if IE 9]></video><![endif]-->'
                        . '<img src="' . $imgFull[0] . '?width=414&height=177&enhance=true" alt="' . trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))) . '" class="lazyload img-responsive">'
                        . '</picture></a>';
                    ?>
                    <div class="grid-paragraph" data-card="corsi">
                        <h3>Costruzioni, Ambiente e Territorio</h3>
                        <p> Gli studenti del corso Costruzioni, ambiente e Territorio (ex
                            geometri) attraverso percorsi tecnici con esercitazioni di laboratorio assistite da
                            insegnanti tecnico pratici, imparano a progettare edifici ed altre strutture grazie il
                            disegno tecnico digitale AutoCAD.</p>
                    </div>
                    <div style="padding: 0 10px 10px 10px">
                    <p align="center"><a
                            href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/costruzioni-ambiente-e-territorio"
                            class="btn btn-info btn-block">Tutti i dettagli</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid-item">
                <?php
                    $imgId = 6232;
                    $imgFull = wp_get_attachment_image_src($imgId, 'full');
                    echo '<a href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/turismo"><picture>'
                        . '<!--[if IE 9]><video style="display: none;"><![endif]-->'
                        . '<source media="(min-width: 992px)" data-srcset="' . $imgFull[0] . '?width=360&height=200&enhance=true">'
                        . '<source media="(min-width: 415px)" data-srcset="' . $imgFull[0] . '?width=777&height=186&enhance=true">'
                        . '<!--[if IE 9]></video><![endif]-->'
                        . '<img src="' . $imgFull[0] . '?width=414&height=177&enhance=true" alt="' . trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))) . '" class="lazyload img-responsive">'
                        . '</picture></a>';
                    ?>
                    <div class="grid-paragraph" data-card="corsi">
                        <h3>Turismo</h3>
                        <p>Nell'indirizzo Turismo si aggiungono la geografia del turismo, la
                            storia dell'arte, le discipline turistico-aziendali. Le materie caratterizzanti del corso
                            costruzioni, ambiente e territoriosono progettazione, costruzioni e impianti, topografia,
                            estimo, sicurezza nel cantiere.</p>
                    </div>
                    <div style="padding: 0 10px 10px 10px">
                        <p align="center"><a href="http://www.istitutotecnicoacerbope.edu.it/corsi-di-studio/turismo"
                                         class="btn btn-info btn-block">Tutti i dettagli</a>
                    </div>
                    </p>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1 post-bodycopy">
                <h2>Discipline caratterizzanti</h2>
                <p>Le discipline di studio caratterizzanti l'indirizzo <strong>Amministrazione, Finanza e Marketing</strong> sono l'economia aziendale, il diritto, l'informatica, 4 lingue Straniere, le relazioni internazionali per il marketing.</p>
                <p>Nell'Indirizzo <strong>Turistico</strong> si aggiungono alle 4 lingue, la geografia del turismo, la storia dell'arte, le discipline turistico –aziendali, diritto e legislazione turistica.</p>
                <p>Le materie caratterizzanti del <strong>Corso Costruzioni, Ambiente e Territorio</strong> sono progettazione, costruzioni e impianti, topografia, estimo, sicurezza nel cantiere. Gli studenti del corso Costruzioni (ex geometri) attraverso percorsi tecnici con esercitazioni di laboratorio assistite da insegnanti tecnico pratici, imparano a progettare edifici ed altre strutture grazie il disegno tecnico digitale AutoCAD (bidimensionale) e Archicad (tridimensionale), inoltre conseguono competenze precise nella gestione della sicurezza e delle risorse energetiche e possono specializzarsi nelle costruzioni in legno o nella geotecnica.</p>
                <h2>La scuola e il lavoro</h2>
                <p>In tutti corsi dell'Istituto tecnico economico e di quello tecnologico al centro di tutte le attività curricolari ed extra curricolari si pone l'esperienza dell'<strong>Alternanza Scuola Lavoro</strong>. A partire dal terzo anno i nostri alunni dedicano nel 150 ore curricolari alla realizzazione e gestione di una Impresa Formativa Simulata e svolgono corsi di formazione a supporto del futuro inserimento nel mondo del lavoro, imparano come si elabora un curricolo europeo anche in rapporto alla Web Reputation, conseguono una formazione certificata sulla sicurezza, indispensabile per iniziare qualsiasi tirocinio o professione.</p>
                <p>Tutti questi interventi formativi di arricchimento del curricolo scolastico ed ampliamento dell'offerta formativa permettono agli studenti dell'Acerbo di affrontare nel quarto e quinto anno di corso <strong>tirocini di lavoro di 4-5 settimane</strong> sia in Italia che all'estero presso studi professionali, enti pubblici, aziende private, hotels, agenzie di viaggio.</p>
            </div>
        </div>

        <?php
        wp_link_pages(array(
            'before' => __('<p class="post-pagination">Pages:', 'montezuma'),
            'after' => '</p>'
        ));
        ?>


    </div>
</div>

<?php get_footer(); ?>

