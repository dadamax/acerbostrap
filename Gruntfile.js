module.exports = function (grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        notify_hooks: {
            options: {
                enabled: true,
                success: true,
                duration: 3
            }
        },
        uglify: {
            custom_mappings: {
                files: [{
                    expand: true, // Enable dynamic expansion.
                    cwd: 'assets/js/custom/dist/', // Src matches are relative to this path.
                    src: ['**/*.js'], // Actual pattern(s) to match.
                    dest: 'build/js/', // Destination path prefix.
                    ext: '.min.js', // Dest filepaths will have this extension.
                    extDot: 'first' // Extensions in filenames begin after the first dot
                }
                ]
            },
            lib_mappings: {
                files: [{
                    expand: true, // Enable dynamic expansion.
                    cwd: 'assets/js/lib/dist/', // Src matches are relative to this path.
                    src: ['**/*.js'], // Actual pattern(s) to match.
                    dest: 'assets/js/lib/min/', // Destination path prefix.
                    ext: '.min.js', // Dest filepaths will have this extension.
                    extDot: 'last' // Extensions in filenames begin after the first dot
                }
                ]
            }
        },
        concat: {
            options: {
                stripBanners: {block: true},
                separator: '\n /* ----- */ \n'
            },
            js: {
                files: {
                    'build/js/blocking.min.js': ['assets/js/jquery-2.1.3.min.js', 'build/js/acerbo.min.js', 'assets/js/lib/min/lazysizes.min.js'],
                    'build/js/matchMediaIE9.min.js': ['assets/js/lib/min/matchMediaIE9.min.js'],
                    'build/js/blockingie8.min.js': ['build/js/acerbo.min.js'],
                    //'build/js/head.min.js': ['assets/js/min/head.min.js'],
                    'build/js/header.min.js': ['assets/js/lib/min/formstone/core.min.js','assets/js/lib/min/formstone/mediaquery.min.js'],
                    'build/js/home.min.js': ['assets/js/lib/min/jquery.lifestream.custom.min.js', 'assets/js/lib/min/owl.carousel.min.js', 'assets/js/min/appuntamenti.min.js'],
                    'build/js/footer.min.js': ['assets/js/lib/min/picturefill.min.js', 'assets/js/lib/min/bootstrap.min.js', 'assets/js/lib/min/jquery.hoverIntent.min.js', 'assets/js/lib/min/smooth-scroll.min.js', 'assets/js/lib/min/waypoints.min.js', 'assets/js/lib/min/waypoints-sticky.min.js', 'assets/js/lib/min/jquery.hypher.min.js', 'assets/js/lib/min/jquery.hypher.it.min.js', 'assets/js/lib/min/cookiebanner.min.js'],
                    'build/js/footerie8.min.js': ['assets/js/lib/min/picturefill.min.js', 'assets/js/lib/min/bootstrap.min.js', 'assets/js/lib/min/hoverintent.min.js', 'assets/js/lib/min/smooth-scroll.min.js', 'assets/js/lib/min/waypoints.min.js', 'assets/js/lib/min/waypoints-sticky.min.js'],
                    'build/js/fs.navigation.min.js': ['assets/js/lib/min/formstone/swap.min.js','assets/js/lib/min/formstone/navigation.min.js'],
                    'build/js/fs.lightbox.min.js': ['assets/js/lib/min/formstone/touch.min.js','assets/js/lib/min/formstone/transition.min.js','assets/js/lib/min/formstone/lightbox.min.js'],
                    'build/js/calendar.min.js': ['assets/js/lib/min/underscore.min.js', 'assets/js/lib/min/calendar.it-IT.min.js', 'assets/js/lib/min/calendar.min.js']
                }
            },
            css: {
                files: {
                    'build/tmp/all.css': ['assets/css/fonts.css', 'assets/css/bootstrap.css', 'assets/css/formstone/navigation.css', 'assets/css/formstone/lightbox.css','assets/css/style.css', 'assets/css/font-awesome.min.css'],
                    'build/tmp/calendar.css': ['assets/css/calendar.css'],
                    'build/tmp/owl.css': ['assets/css/owl.carousel.css'],
                    'build/tmp/bootstrap.editor.css': ['assets/css/bootstrap.editor.css']
                }
            }
        },
        cssc: {
            csscFirstSet: {
                options: {
                    consolidateViaDeclarations: false,
                    consolidateViaSelectors: true,
                    consolidateMediaQueries: false
                },
                files: {
                    'build/tmp/all.condensed.css': 'build/tmp/all.css',
                    'build/tmp/calendar.condensed.css': 'build/tmp/calendar.css',
                    'build/tmp/owl.condensed.css': 'build/tmp/owl.css',
                    'build/tmp/bootstrap.editor.condensed.css': 'build/tmp/bootstrap.editor.css',
                    'build/tmp/critical.condensed.home.css': 'build/tmp/critical.home.css',
                    'build/tmp/critical.condensed.single.css': 'build/tmp/critical.single.css'
                }
            }
        },
        cssmin: {
            build: {
                files: {
                    'build/css/all.condensed.min.css': 'build/tmp/all.condensed.css',
                    'build/css/calendar.condensed.min.css': 'build/tmp/calendar.condensed.css',
                    'build/css/owl.condensed.min.css': 'build/tmp/owl.condensed.css',
                    'build/admin/bootstrap.editor.condensed.min.css': 'build/tmp/bootstrap.editor.condensed.css',
                    'build/css/critical.condensed.home.min.css': 'build/tmp/critical.condensed.home.css',
                    'build/css/critical.condensed.single.min.css': 'build/tmp/critical.condensed.single.css'
                }
            }
        },
        critical: {
            home: {
                options: {
                    base: './',
                    width: 1280,
                    height: 900,
                    css: [
                        'assets/css/bootstrap.css',
                        'assets/css/style.css',
                        'assets/css/formstone/lightbox.css',
                        'assets/css/formstone/navigation.css',
                        'assets/css/owl.carousel.css',
                        'assets/css/font-awesome.min.css',
                        'assets/css/calendar.css'
                    ],
                    minify: false
                },
                src: 'build/tmp/home.html',
                dest: 'build/tmp/critical.home.css'
            },
            single: {
                options: {
                    base: './',
                    width: 1280,
                    height: 900,
                    css: [
                        'assets/css/bootstrap.css',
                        'assets/css/style.css',
                        'assets/css/formstone/lightbox.css',
                        'assets/css/formstone/navigation.css',
                        'assets/css/owl.carousel.css',
                        'assets/css/font-awesome.min.css',
                        'assets/css/calendar.css'
                    ],
                    minify: false
                },
                src: 'build/tmp/single.html',
                dest: 'build/tmp/critical.single.css'
            }
        },
        criticalcss: {
            home: {
                options: {
                    url: "http://local.acerbo.it/?fsc=yes",
                    width: 1200,
                    height: 900,
                    outputfile: "build/tmp/criticalcss.home.css",
                    filename: "build/tmp/all.css",
                    buffer: 800 * 1024,
                    ignoreConsole: false
                }
            },
            single: {
                options: {
                    url: "http://local.acerbo.it/studenti/1124-iscrizioni-online-tito-acerbo-a-s-20152016/?fsc=yes",
                    width: 1200,
                    height: 900,
                    outputfile: "build/tmp/criticalcss.single.css",
                    filename: "build/tmp/all.css",
                    buffer: 800 * 1024,
                    ignoreConsole: false
                }
            }
        },
        http: {
            home: {
                options: {
                    url: 'http://local.acerbo.it/?fsc=yes'
                },
                dest: 'build/tmp/home.html'
            },
            single: {
                options: {
                    url: 'http://local.acerbo.it/studenti/1124-iscrizioni-online-tito-acerbo-a-s-20152016/?fsc=yes'
                },
                dest: 'build/tmp/single.html'
            }
        },
        "bower-install-simple": {
            options: {
                color: true,
                directory: "lib"
            },
            "prod": {
                options: {
                    production: true
                }
            },
            "dev": {
                options: {
                    production: false
                }
            }
        },
        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: 'version',
                        replacement: '<%= Math.floor((Date.now() / 1000)) %>'
                    }
                    ]
                },
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['assets/critical/*.*'],
                    dest: 'build/critical/'
                }
                ]
            }
        },
        jshint: {
            all: ['assets/js/app/**/*.js']
        },
        imageEmbed: {
            dist: {
                baseDir: "../",
                src: ["build/tmp/all.url.css"],
                dest: "build/tmp/all.base64.css",
                options: {
                    deleteAfterEncoding: false,
                    preEncodeCallback: function (filename) {
                        return true;
                    }
                }
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        }

    });

    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-conventional-changelog');
    grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.registerTask('build', ['uglify']);
    grunt.registerTask('buildjs', ['uglify', 'concat:js', 'replace']);
    grunt.registerTask('buildcss', ['http', 'critical', 'concat:css', 'cssc', 'cssmin', 'replace']);
    grunt.registerTask('buildcsslight', ['concat:css', 'cssc', 'cssmin', 'replace']);
    grunt.registerTask('buildfonts', ['copy:fontawesome', 'copy:adobeblank']);
    grunt.task.run('notify_hooks');

};