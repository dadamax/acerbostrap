<?php
/*
  Template Name: Risultati della ricerca
 */



get_header();
$query = empty($_GET['q']) ? '' : sanitize_text_field($_GET['q']);
?>
<style>
	#content .gsc-selected-option-container {
		width: 100px!important;
	}

	#content .gsc-option-menu-item:hover {
		background-color: #fa2b13;
		color: white;
	}

	#content .gsc-option-menu-item.gsc-option-menu-item-highlighted:hover {
		background-color: #EEE;
		border-color: #EEE;
		border-width: 1px 0;
		color: #333;
	}
</style>
<div class="container">
    <div class="row" style="padding:20px 5px">
        <div class="cf col-md-12">
            <form method="get" class="searchform" action="/serp" id="searchbox_002155363683088257630:rwow5whtbvg">
                <div class="input-group">
                    <input value="002155363683088257630:rwow5whtbvg" name="cx" type="hidden"/>
                    <input value="FORID:11" name="cof" type="hidden"/>
                    <input type="text" class="form-control" name="q" id="q" placeholder='Scrivi il testo e premi Cerca' value="<?php echo $query; ?>">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search fa-lg"></i>&nbsp;<span class="hidden-xs">Cerca</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="container" style="min-height: 500px">
    <div class="row">
        <div id="content" class="cf col-md-12">

	        <!--div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Chiudi"><span aria-hidden="true">×</span></button> <h4>Utilizzo dei cookie</h4> <p>Il motore di ricerca del sito dell'Istituto Acerbo, fornito da Google, potrebbe utilizzare dei cookie profilanti per la gestione dei risultati. Per accettare l'uso dei cookie fai clic sul pulsante qui sotto. In alternativa puoi visualizzare i risultati di ricerca utilizzando un servizio non fornito da Google che potrebbe rivelarsi tuttavia meno preciso. </p> <p> <button type="button" class="btn btn-primary accept_cookie">Accetta e mostra i risultati</button></p> </div-->


	        <script>
		        (function() {
			        var cx = '002155363683088257630:rwow5whtbvg';
			        var gcse = document.createElement('script');
			        gcse.type = 'text/javascript';
			        gcse.async = true;
			        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
				        '//cse.google.com/cse.js?cx=' + cx;
			        var s = document.getElementsByTagName('script')[0];
			        s.parentNode.insertBefore(gcse, s);
		        })();
	        </script>
	        <gcse:searchresults-only></gcse:searchresults-only>
        </div>
    </div>
</div>




<?php
//                try {
//                     $parameters['q'] = "fullText contains 'caccavella'";
//                    $lol = $drive->files->listFiles($parameters);
//                } catch (Exception $e) {
//                    echo "An error occurred: " . $e->getMessage();
//                }
//
//                print_r($lol);
?>

<?php get_footer(); ?>