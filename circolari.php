<?php
define( 'WP_USE_THEMES', false );
require( './wp-load.php' );


if ( isset( $_GET['np'] ) && is_numeric( $_GET['np'] ) ) {
	$posts_per_page = $_GET['np'];
} else {
	$posts_per_page = 10;
}

$query = new WP_Query( array(
	'posts_per_page' => $posts_per_page,
	'post_type'      => array( 'circolare', 'avviso' ),
) );


?>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/journal/bootstrap.min.css" rel="stylesheet"
	      integrity="sha256-bPSyJP9+ovy9/YbIwvZyX6r2M4PkiF01vt0zuDLUEzs= sha512-F4ep7hh9V7i7UvRF56Vq8VgjFIFGRVaE2y5UZ2FtIcMWoLFiDxTmHgnDm1XDZGcc6Vtp+kwtL0Kd3VA8uBMK9g=="
	      crossorigin="anonymous">

	<style type="text/css">
		hr.styled {
			border: 0;
			height: 10px;
			width: 95%;
			margin-left: auto;
			margin-right: auto;
			background: url('/wp-content/themes/acerbostrap/images/sprite_lg.png') center -10.5px;
		}

		.content {
			padding: 20px;
		}

	</style>
</head>
<body>
<div class="container-fluid">
	<!-- Example row of columns -->
	<div class="row">
		<div class="content">
			<?php
			$mypost = $query->get_posts();
			if ( $mypost ) {
				foreach ( $mypost as $post ) : setup_postdata( $post );
					?>

					<h2>
						<?php
						if ( $post->post_type == 'circolare' ) {
							$num  = CFS()->get( 'circolare_num' );
							$data = CFS()->get( 'circolare_data' );
							echo '<small>Circolare ' . $num . ' del ' . date_i18n( 'j F Y', strtotime( $data ) ) . '</small>';
						} else {
							$num  = CFS()->get( 'avviso_num' );
							$data = CFS()->get( 'avviso_data' );
							echo '<small>Avviso ' . $num . ' del ' . date_i18n( 'j F Y', strtotime( $data ) ) . '</small>';
						}
						?>
						<br><?php the_title(); ?></h2>
					<?php the_excerpt(); ?>
					<hr class="styled">
					<?php
				endforeach;
				wp_reset_postdata();
			}
			?>
		</div>
	</div>
</div>
</body>
</html>