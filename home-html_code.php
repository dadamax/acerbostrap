<?php
$code = ot_get_option('hp_html_code');
$title = ot_get_option('open_content_title');
?>


<div class="col-sm-12">
    <h1 class="title compensate-bs"><?php echo $title; ?></h1>
    <div class="cf compensate-bs">
        <?php echo $code; ?>
    </div>
</div>
